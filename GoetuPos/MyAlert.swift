//
//  MyAlert.swift
//  Go3Reservation
//
//  Created by Luigi Guevarra on 5/7/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit

class MyAlert {
    @available(iOS 8.0, *)
    class func displayAlert(_ title:String, msg:String, controller: UIViewController) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        //        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
        //            controller.dismissViewControllerAnimated(true, completion: nil)
        //        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    class func displayAlertWithDismiss(_ title:String, msg:String, controller: UIViewController) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            let _ = controller.navigationController?.popViewController(animated: true)
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    class func displayAlertWithModalDismiss(_ title:String, msg:String, controller: UIViewController) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            controller.dismiss(animated: false, completion: nil)
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
}
