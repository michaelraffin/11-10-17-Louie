//
//  ModalPaymentReceipt.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 10/4/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//




import UIKit

protocol ModalPaymentReceiptViewControllerDelegate {
    func didOpenModalPaymentReceipt(viewController vc: ModalPaymentReceipt, success: Bool)
}

class ModalPaymentReceipt: UIViewController {
    
    
    @IBOutlet weak var btnPrintReceipt: UIButton!
    @IBOutlet weak var btnNoReceipt: UIButton!
//    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblChange: UILabel!
    
    var validate = Validation()
    var progressView = CustomProgressView()
    //var RecordList: [[String:String]] = [[String:String]]()
    var selectedIndex = 0
    var isEdit:Bool = false
    
    let date = Date()
    let format = DateFormatter()
    
    var pos_transaction_header = PosTransactionHeader()
    //var pos_transaction_detail = [PosTransactionDetail]()
    //var pos_transaction_payment = PosTransactionPayment()
    var change:Double = 0
    
    var protocolDelegate : ModalPaymentReceiptViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print
        let x_pos = (btnPrintReceipt.frame.size.width / 2) - 16
        
        let leftImageView1 = UIImageView()
        leftImageView1.image = UIImage(named: "print")
        leftImageView1.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnPrintReceipt.addSubview(leftImageView1)
        
        //no
        let leftImageView2 = UIImageView()
        leftImageView2.image = UIImage(named: "no")
        leftImageView2.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnNoReceipt.addSubview(leftImageView2)
        
        self.lblChange.text = UIUtils.formatCurrency(value: change, style: .currency)
        //cancel
        
//        let leftImageView3 = UIImageView()
//        leftImageView3.image = UIImage(named: "cancel")
//        leftImageView3.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
//        
//        btnCancel.addSubview(leftImageView3)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func btnPrintReceipt(_ sender: Any) {
        
        self.protocolDelegate?.didOpenModalPaymentReceipt(viewController: self, success: true)

        
        
    }
    
    @IBAction func btnNoReceipt(_ sender: Any) {
        
        
        self.protocolDelegate?.didOpenModalPaymentReceipt(viewController: self, success: true)

        
    }
    
//    @IBAction func btnCancel(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
//    }
    
    
    
    
    
    
    
} // end class
