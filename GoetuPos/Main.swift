//
//  Main.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/10/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit


class SpecialistCell: UICollectionViewCell{
    

    @IBOutlet weak var lblSpecialistName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    
    
    @IBOutlet weak var imgSpecialistPicture: UIImageView!
    
}

class InServiceCell: UICollectionViewCell{
    
    
    @IBOutlet weak var lblSpecialistName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    
    @IBOutlet weak var imgSpecialistPicture: UIImageView!
    
}



class Main: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {


    
    
    @IBOutlet weak var btnFunction: UIButton!
    @IBOutlet weak var btnMove: UIButton!
    @IBOutlet weak var btnCheckIn: UIButton!
    @IBOutlet weak var btnAppointment: UIButton!
    @IBOutlet weak var btnWaitList: UIButton!
    @IBOutlet weak var btnFastSale: UIButton!
    @IBOutlet weak var btnRefund: UIButton!
    @IBOutlet weak var btnExit: UIButton!

    
    @IBOutlet weak var btnAppointmentList: UIButton!
    @IBOutlet weak var btnInServiceList: UIButton!
    @IBOutlet weak var btnWaitingList: UIButton!
    
    
    var not_selected_color: UIColor!
    var selected_color: UIColor!
    
    var specialistList: [[String:String]] = [[String:String]]()
    
    var inServiceList: [[String:String]] = [[String:String]]()
    
    var progressView = CustomProgressView()
    
    
    let date = Date()
    let format = DateFormatter()
    
    @IBOutlet weak var colVwSpecialist: UICollectionView!
    
    @IBOutlet weak var colVwInService: UICollectionView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        self.not_selected_color = btnAppointmentList.backgroundColor
        self.selected_color = btnInServiceList.backgroundColor
        


        
        
        self.get_active_specialist_on_queue()
        self.get_in_service_specialist_on_queue()
    }
    
    
    func setupButtons(){
        
    let x_pos = (btnFunction.frame.size.width / 2) - 16
    
    let leftImageView1 = UIImageView()
    leftImageView1.image = UIImage(named: "function")
    leftImageView1.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnFunction.addSubview(leftImageView1)
    
    let leftImageView2 = UIImageView()
    leftImageView2.image = UIImage(named: "move")
    leftImageView2.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnMove.addSubview(leftImageView2)
    
    let leftImageView3 = UIImageView()
    leftImageView3.image = UIImage(named: "check_in")
    leftImageView3.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnCheckIn.addSubview(leftImageView3)
    
    let leftImageView4 = UIImageView()
    leftImageView4.image = UIImage(named: "appointment")
    leftImageView4.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnAppointment.addSubview(leftImageView4)
    
    let leftImageView5 = UIImageView()
    leftImageView5.image = UIImage(named: "waitlist")
    leftImageView5.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnWaitList.addSubview(leftImageView5)
    
    let leftImageView6 = UIImageView()
    leftImageView6.image = UIImage(named: "fastsale")
    leftImageView6.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnFastSale.addSubview(leftImageView6)
    
    let leftImageView7 = UIImageView()
    leftImageView7.image = UIImage(named: "refund")
    leftImageView7.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnRefund.addSubview(leftImageView7)
    
    let leftImageView8 = UIImageView()
    leftImageView8.image = UIImage(named: "exit")
    leftImageView8.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
    
    btnExit.addSubview(leftImageView8)
    
    
    }
    
    override func viewDidAppear(_ animated: Bool) {

        //print("subviews = \()")
        
        if(self.btnFunction.subviews.count == 1){
            setupButtons()
        }

        

    }
    
    func get_active_specialist_on_queue(){
        
        if (self.specialistList.count > 0) {
            self.specialistList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select spq.*, e.first_name, e.last_name, e.profile_image from specialist_on_queue spq left join employee e on (e.id = spq.specialist_id) where spq.status = 'A'  order by order_number asc"
        
        let db = Database()

        let rs = db.getResults(cmd)
        
        
        
        while (rs.next()) {
            
            
            let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
            let first_name = rs.string(forColumn: "first_name") ?? ""
            let last_name = rs.string(forColumn: "last_name") ?? ""
            let profile_image = rs.string(forColumn: "profile_image") ?? ""
            let order_number = rs.string(forColumn: "order_number") ?? ""
            let total_count = rs.string(forColumn: "total_count") ?? ""
            let total_amount = rs.string(forColumn: "total_amount") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            
                
                let obj = [
                    "specialist_id":"\(specialist_id)",
                    "first_name":"\(first_name)",
                    "last_name":"\(last_name)",
                    "profile_image":"\(profile_image)",
                    "order_number":"\(order_number)",
                    "total_count":"\(total_count)",
                    "total_amount":"\(total_amount)",
                    "status":"\(status)",
                    "transaction_id":"-1",
                ]
                specialistList.append(obj)
                
            

        }
        

        
        self.colVwSpecialist.reloadData()
        
        db.closeDB()
    }
    
    func get_in_service_specialist_on_queue(){
        
        
        if (self.inServiceList.count > 0) {
            self.inServiceList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select spq.*, e.first_name, e.last_name, e.profile_image from specialist_on_queue spq left join employee e on (e.id = spq.specialist_id) where spq.status = 'I' order by order_number asc"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        
        
        while (rs.next()) {
            
            
            let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
            let first_name = rs.string(forColumn: "first_name") ?? ""
            let last_name = rs.string(forColumn: "last_name") ?? ""
            let profile_image = rs.string(forColumn: "profile_image") ?? ""
            let order_number = rs.string(forColumn: "order_number") ?? ""
            let total_count = rs.string(forColumn: "total_count") ?? ""
            let total_amount = rs.string(forColumn: "total_amount") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            let transaction_id = rs.string(forColumn: "transaction_id") ?? ""
            
                
                let obj = [
                    "specialist_id":"\(specialist_id)",
                    "first_name":"\(first_name)",
                    "last_name":"\(last_name)",
                    "profile_image":"\(profile_image)",
                    "order_number":"\(order_number)",
                    "total_count":"\(total_count)",
                    "total_amount":"\(total_amount)",
                    "status":"\(status)",
                    "transaction_id":"\(transaction_id)",
                ]
                inServiceList.append(obj)
                
            
            
            
        }
        
        
        self.colVwInService.reloadData()
        db.closeDB()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView == colVwSpecialist){
            return specialistList.count
        }else{
            return inServiceList.count
        }
        

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == colVwSpecialist){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecialistCell", for: indexPath) as! SpecialistCell
            
            let row = indexPath.row
            let obj = specialistList[row] as NSDictionary
            
            let specialist_name = specialistList[row]["first_name"]! + " " + specialistList[row]["last_name"]!
            let amount = obj["total_amount"] as! String
            let count = obj["total_count"] as! String
            let order_number = obj["order_number"] as! String
            let picture = obj["profile_image"] as! String
            
//            cell.vwCell.layer.borderWidth = 1
//            cell.vwCell.layer.borderColor = UIUtils.colorWithHexString(hex: "#EDEDED").cgColor
            
            cell.lblSpecialistName.text = specialist_name
            cell.lblAmount.text = UIUtils.formatCurrency(value: Double(amount)!, style: .currency)
            cell.lblCount.text = "Count = " + count
            cell.lblNo.text = "# " + order_number
            cell.tag = row
            
            
            
            let dataDecoded = NSData(base64Encoded: picture, options: NSData.Base64DecodingOptions(rawValue: 0))
            let decodedimage = UIImage(data: dataDecoded! as Data)
            
            //dataDecoded
            
            if(decodedimage == nil){
                cell.imgSpecialistPicture.image = UIImage(named: "user")
            }else{
                cell.imgSpecialistPicture.image = decodedimage
            }
            

            //cell.imgSpecialistPicture.layer.masksToBounds = true
            
            return cell
            
            
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InServiceCell", for: indexPath) as! InServiceCell
            let row = indexPath.row
            let obj = inServiceList[row] as NSDictionary
            
            let specialist_name = inServiceList[row]["first_name"]! + " " + inServiceList[row]["last_name"]!
            let amount = obj["total_amount"] as! String
            let count = obj["total_count"] as! String
            let order_number = obj["order_number"] as! String
            let picture = obj["profile_image"] as! String
            
            //            cell.vwCell.layer.borderWidth = 1
            //            cell.vwCell.layer.borderColor = UIUtils.colorWithHexString(hex: "#EDEDED").cgColor
            
            cell.lblSpecialistName.text = specialist_name
            cell.lblAmount.text = UIUtils.formatCurrency(value: Double(amount)!, style: .currency)
            cell.lblCount.text = "Count = " + count
            cell.lblNo.text = "# " + order_number
            cell.tag = row
            
            
            
            let dataDecoded = NSData(base64Encoded: picture, options: NSData.Base64DecodingOptions(rawValue: 0))
            let decodedimage = UIImage(data: dataDecoded! as Data)
            
            //dataDecoded
            
            if(decodedimage == nil){
                cell.imgSpecialistPicture.image = UIImage(named: "user")
            }else{
                cell.imgSpecialistPicture.image = decodedimage
            }
            
            
            //cell.imgSpecialistPicture.layer.masksToBounds = true
            
            return cell
            
            
            
        }
        
        
        
        
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("row number \(indexPath.row) specialist_name = \(specialistList[indexPath.row]["first_name"]!) \(specialistList[indexPath.row]["last_name"]!)")
        
        
        if(collectionView == colVwSpecialist){
            self.OpenTicket(index: indexPath.row, type: "", is_fast_sale: false)
        }else{
            self.OpenTicket(index: indexPath.row, type: "in_service", is_fast_sale: false)
        }
        
        
        
        
    }
    
    
    @IBAction func btnAppointmentList(_ sender: Any) {
        btnAppointmentList.backgroundColor = self.selected_color
        btnInServiceList.backgroundColor = self.not_selected_color
        btnWaitingList.backgroundColor = self.not_selected_color
    }

    @IBAction func btnInServiceList(_ sender: Any) {
        btnAppointmentList.backgroundColor = self.not_selected_color
        btnInServiceList.backgroundColor = self.selected_color
        btnWaitingList.backgroundColor = self.not_selected_color
    }

    @IBAction func btnWaitingList(_ sender: Any) {
        btnAppointmentList.backgroundColor = self.not_selected_color
        btnInServiceList.backgroundColor = self.not_selected_color
        btnWaitingList.backgroundColor = self.selected_color
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnFunction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Function", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Function") as! Function
        vc.protocolDelegate = self
       // vc.modalPresentationStyle = .overCurrentContext
        
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ModalCategories") as! ModalCategories
//        vc.protocolDelegate = self
//        
//        vc.category_info.id = ""
//        vc.category_info.name = ""
//        vc.category_info.notes = ""
//        vc.isEdit = false
        
        
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func btnMove(_ sender: Any) {
        //test air print
//        var pInfo:UIPrintInfo = UIPrintInfo.printInfo()
//        pInfo.outputType = UIPrintInfoOutputType.general
//        pInfo.jobName = (webView.request?.url?.absoluteString)!
//        pInfo.orientation = UIPrintInfoOrientation.portrait
//        
//        var printController = UIPrintInteractionController.shared
//        printController.printInfo = pInfo
//        printController.showsPageRange = true
//        printController.printFormatter = webView.viewPrintFormatter()
//        printController.present(animated: true, completionHandler: nil)
        
        
    }
    
    @IBAction func btnCheckIn(_ sender: Any) {
        
    }
    
    @IBAction func btnAppointmentBook(_ sender: Any) {
        
    }
    
    @IBAction func btnWaitList(_ sender: Any) {
        
    }

    @IBAction func btnFastSale(_ sender: Any) {
        self.OpenTicket(index: 0, type: "", is_fast_sale: true)
    }
    
    @IBAction func btnRefund(_ sender: Any) {
        
    }
    
    @IBAction func btnExit(_ sender: Any) {
        
    }

    
    func OpenTicket(index:Int, type:String, is_fast_sale:Bool){
        
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Ticket") as! Ticket
        vc.protocolDelegate = self
        
        if(!is_fast_sale){
            
            if(type == ""){
                vc.employee.id = self.specialistList[index]["specialist_id"]!
                vc.employee.user_id = "1"
                vc.employee.commision_id = "-1"
                vc.employee.last_name = self.specialistList[index]["last_name"]!
                vc.employee.first_name = self.specialistList[index]["first_name"]!
                vc.employee.middle_name = ""
                vc.employee.mobile_number = ""
            }else{
                
                
                vc.employee.id = self.inServiceList[index]["specialist_id"]!
                vc.employee.user_id = "1"
                vc.employee.commision_id = "-1"
                vc.employee.last_name = self.inServiceList[index]["last_name"]!
                vc.employee.first_name = self.inServiceList[index]["first_name"]!
                vc.employee.middle_name = ""
                vc.employee.mobile_number = ""
                
                var cmd = ""
                
                cmd = "select pth.*, ifnull(d.is_percent,'0') as discount_is_percent, ifnull(d.value,'0') as discount_value from pos_transaction_header pth left join pos_discounts d on (d.id = pth.discount_id) where pth.id = \(self.inServiceList[index]["transaction_id"]!)"
                
                let db = Database()
                
                var rs:FMResultSet! = nil
                
                rs = db.getResults(cmd)
                
                while (rs.next()) {
                    
                    vc.pos_transaction_header.id = rs.string(forColumn: "id") ?? ""
                    vc.pos_transaction_header.doc_code = rs.string(forColumn: "doc_code") ?? ""
                    vc.pos_transaction_header.trans_code = rs.string(forColumn: "trans_code") ?? ""
                    vc.pos_transaction_header.type_code = rs.string(forColumn: "type_code") ?? ""
                    vc.pos_transaction_header.site_code = rs.string(forColumn: "site_code") ?? ""
                    
                    vc.pos_transaction_header.status = rs.string(forColumn: "status") ?? ""
                    vc.pos_transaction_header.terminal_id = rs.string(forColumn: "terminal_id") ?? ""
                    vc.pos_transaction_header.reference_number = rs.string(forColumn: "reference_number") ?? ""
                    vc.pos_transaction_header.remarks = rs.string(forColumn: "remarks") ?? ""
                    vc.pos_transaction_header.discount_id = rs.string(forColumn: "discount_id") ?? ""
                    
                    vc.pos_transaction_header.customer_id = rs.string(forColumn: "customer_id") ?? "-1"
                    vc.pos_transaction_header.specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                    vc.pos_transaction_header.total_discount = rs.string(forColumn: "total_discount") ?? ""
                    vc.pos_transaction_header.total_item_discount = rs.string(forColumn: "total_item_discount") ?? ""
                    vc.pos_transaction_header.total_tax = rs.string(forColumn: "total_tax") ?? ""
                    vc.pos_transaction_header.total_tax_exempt = rs.string(forColumn: "total_tax_exempt") ?? ""
                    
                    vc.pos_transaction_header.total_tip = rs.string(forColumn: "total_tip") ?? ""
                    vc.pos_transaction_header.tip_is_percent = rs.string(forColumn: "tip_is_percent") ?? ""
                    vc.pos_transaction_header.total_gross = rs.string(forColumn: "total_gross") ?? ""
                    vc.pos_transaction_header.total_net = rs.string(forColumn: "total_net") ?? ""
                    vc.pos_transaction_header.create_date = rs.string(forColumn: "create_date") ?? ""
                    vc.pos_transaction_header.create_by = rs.string(forColumn: "create_by") ?? ""
                    
                    vc.pos_transaction_header.update_date = rs.string(forColumn: "update_date") ?? ""
                    vc.pos_transaction_header.update_by = rs.string(forColumn: "update_by") ?? ""
                    vc.pos_transaction_header.cancel_date = rs.string(forColumn: "cancel_date") ?? ""
                    vc.pos_transaction_header.cancel_by = rs.string(forColumn: "cancel_by") ?? ""

                    vc.pos_transaction_header.discount_is_percent = rs.string(forColumn: "discount_is_percent") ?? ""
                    vc.pos_transaction_header.discount_value = rs.string(forColumn: "discount_value") ?? ""
                    
                }

                
                cmd = "select ptd.* ,e.first_name as first_name, e.last_name as last_name, ifnull(pd.name,'') as discount_name, ifnull(pd.is_percent,'') as discount_is_percent, ifnull(pd.value, '') as discount_value, case ptd.main_category_id when 1 then s.name when 2 then p.name when 3 then pa.name when 4 then g.name end as item_name  from pos_transaction_detail ptd left join employee e on (e.id = ptd.specialist_id) left join pos_discounts pd on (pd.id = ptd.discount_id) left join services s on (s.id = ptd.item_id) left join products p on (p.id = ptd.item_id) left join packages pa on (pa.id = ptd.item_id) left join gift_cards g on (g.id = ptd.item_id) where ptd.id = \(self.inServiceList[index]["transaction_id"]!)"
                
                
                
                rs = db.getResults(cmd)
                
                while (rs.next()) {
                    
                    let detail = PosTransactionDetail()
                    
                    detail.id = rs.string(forColumn: "id") ?? ""
                    detail.line_number = rs.string(forColumn: "line_number") ?? ""
                    detail.specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                    detail.commision_id = rs.string(forColumn: "commision_id") ?? ""
                    detail.main_category_id = rs.string(forColumn: "main_category_id") ?? ""
                    detail.item_id = rs.string(forColumn: "item_id") ?? ""
                    detail.quantity = rs.string(forColumn: "quantity") ?? ""
                    detail.price = rs.string(forColumn: "price") ?? ""
                    detail.discount_id = rs.string(forColumn: "discount_id") ?? ""
                    detail.discount_is_percent = rs.string(forColumn: "discount_is_percent") ?? ""
                    detail.discount_value = rs.string(forColumn: "discount_value") ?? ""
                    detail.item_discount = rs.string(forColumn: "item_discount") ?? ""
                    detail.item_gross = rs.string(forColumn: "item_gross") ?? ""
                    detail.item_net = rs.string(forColumn: "item_net") ?? ""
                    detail.item_remarks = rs.string(forColumn: "item_remarks") ?? ""
                    
                    detail.first_name = rs.string(forColumn: "first_name") ?? ""
                    detail.last_name = rs.string(forColumn: "last_name") ?? ""
                    detail.item_name = rs.string(forColumn: "item_name") ?? ""

                    
                    vc.pos_transaction_detail.append(detail)
                    
                }
                
                
                //pos customer
                
                cmd = "select * from pos_customers where id = \(vc.pos_transaction_header.customer_id)"
                
                rs = db.getResults(cmd)
                
                while (rs.next()) {
                    
                    
                    vc.pos_customer.id = rs.string(forColumn: "id") ?? "-1"
                    vc.pos_customer.first_name = rs.string(forColumn: "first_name") ?? ""
                    vc.pos_customer.last_name = rs.string(forColumn: "last_name") ?? ""
                    vc.pos_customer.status = rs.string(forColumn: "status") ?? ""
                    vc.pos_customer.notes = rs.string(forColumn: "notes") ?? ""
                    vc.pos_customer.gender = rs.string(forColumn: "gender") ?? ""
                    vc.pos_customer.civil_status = rs.string(forColumn: "civil_status") ?? ""
                    vc.pos_customer.address = rs.string(forColumn: "address") ?? ""
                    vc.pos_customer.state_id = rs.string(forColumn: "state_id") ?? "-1"
                    vc.pos_customer.city_id = rs.string(forColumn: "city_id") ?? "-1"
                    vc.pos_customer.zip_code = rs.string(forColumn: "zip_code") ?? ""
                    vc.pos_customer.member_id = rs.string(forColumn: "member_id") ?? ""
                    vc.pos_customer.mobile_number = rs.string(forColumn: "mobile_number") ?? ""
                    vc.pos_customer.email_address = rs.string(forColumn: "email_address") ?? ""
                    vc.pos_customer.birthdate = rs.string(forColumn: "birthdate") ?? ""
                    vc.pos_customer.default_kit_id = rs.string(forColumn: "default_kit_id") ?? "-1"
                    vc.pos_customer.first_visit_date = rs.string(forColumn: "first_visit_date") ?? ""
                    vc.pos_customer.last_visit_date = rs.string(forColumn: "last_visit_date") ?? ""
                    vc.pos_customer.visit_count = rs.string(forColumn: "visit_count") ?? "0"
                    vc.pos_customer.total_spent = rs.string(forColumn: "total_spent") ?? "0"
                    vc.pos_customer.loyalty_points = rs.string(forColumn: "loyalty_points") ?? "0"
                    vc.pos_customer.create_date = rs.string(forColumn: "create_date") ?? ""
                    vc.pos_customer.create_by = rs.string(forColumn: "create_by") ?? ""
                    vc.pos_customer.update_date = rs.string(forColumn: "update_date") ?? ""
                    vc.pos_customer.update_by = rs.string(forColumn: "update_by") ?? ""

                    
                }
                
                
                
                db.closeDB()
                
            }

            
        }

        
        self.present(vc, animated: false, completion: nil)
    }
    
    
    
    
    
} // class end

extension Main:FunctionViewControllerDelegate {
    func didOpenFunction(viewController vc: Function, success: Bool)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)
            self.get_active_specialist_on_queue()
            
            //self.product_id = wait_list_info.product_id
        }
        
    }
}

extension Main:TicketViewControllerDelegate {
    func didOpenTicket(viewController vc: Ticket, success: Bool)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        if success
        {
            vc.dismiss(animated: true, completion: nil)
            self.get_active_specialist_on_queue()
            self.get_in_service_specialist_on_queue()
            //self.product_id = wait_list_info.product_id
        }
        
    }
}

