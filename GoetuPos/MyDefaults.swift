//
//  MyDefaults.swift
//  Go3Reservation
//
//  Created by Luigi Guevarra on 5/1/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import Foundation

class MyDefaults {
    
    class func saveString(_ key: String!, value: String!) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        
    }
    
    class func removeString(_ key: String!) {
        let defaults = UserDefaults.standard
        //        defaults.setObject(nil, forKey: key)
        defaults.removeObject(forKey: key)
    }
    
    class func getString(_ key: String!) -> String! {
        let defaults = UserDefaults.standard
        let str = defaults.string(forKey: key)
        return str
    }
    
    
    class func getTax(){
        let db = Database()
        let cmd = "select * from pos_system_settings where notes = 'tax'"
        
        let rs = db.getResults(cmd)
        
        if rs.next() == true {
            
            let value = rs.string(forColumn: "value")
            
            MyDefaults.saveString("tax", value: value)
            
        } else {
            MyDefaults.saveString("tax", value: "0")
        }
        
        db.closeDB()
    }
    
    
}
