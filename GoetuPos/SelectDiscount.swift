//
//  SelectDiscount.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/16/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit

protocol SelectDiscountViewControllerDelegate {
    func didOpenSelectDiscount(viewController vc: SelectDiscount, success: Bool, discount_id: String, value : String, is_percent : String)
}

class DiscountCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDiscountName: UILabel!
    
}

class SelectDiscount: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var colVw: UICollectionView!
    
    var protocolDelegate : SelectDiscountViewControllerDelegate?
    
    var is_ticket = false

    var PosDiscountList: [[String:String]] = [[String:String]]()
    
    var progressView = CustomProgressView()
    
    let date = Date()
    let format = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPosDiscounts()

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PosDiscountList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscountCell", for: indexPath) as! DiscountCell
        
        let row = indexPath.row
        let obj = PosDiscountList[row] as NSDictionary
        
        //let id = obj["id"] as! String
        let name = obj["name"] as! String
        //

        cell.lblDiscountName.text = name

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        self.protocolDelegate?.didOpenSelectDiscount(viewController: self, success: true, discount_id: self.PosDiscountList[indexPath.row]["id"]!, value: self.PosDiscountList[indexPath.row]["value"]!, is_percent: self.PosDiscountList[indexPath.row]["is_percent"]!)
        
//        self.protocolDelegate?.didOpenTicketItems(viewController: self, main_category_id: self.selected_main_category_id, item_id: self.ItemList[indexPath.row]["id"]!, price: self.ItemList[indexPath.row]["price"]!, item_name: self.ItemList[indexPath.row]["name"]!)
        
        
    }
    
    func getPosDiscounts(){
        
        if (self.PosDiscountList.count > 0) {
            self.PosDiscountList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from pos_discounts where status = 'A'"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            let id = rs.string(forColumn: "id") ?? ""
            let name = rs.string(forColumn: "name") ?? ""
            let notes = rs.string(forColumn: "notes") ?? ""
            let value = rs.string(forColumn: "value") ?? ""
            let is_percent = rs.string(forColumn: "is_percent") ?? ""
            
            
            let obj = [
                "id":"\(id)",
                "name":"\(name)",
                "notes":"\(notes)",
                "value":"\(value)",
                "is_percent":"\(is_percent)"
            ]
            self.PosDiscountList.append(obj)
            
            
            
            
        }
        
        
        self.colVw.reloadData()
        //    self.colVwSpecialist.reloadData()
        //    self.colVwInService.reloadData()
        db.closeDB()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    



}
