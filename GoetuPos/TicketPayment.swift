//
//  TicketPayment.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/18/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit

protocol TicketPaymentViewControllerDelegate {
    func didOpenTicketPayment(viewController vc: TicketPayment, success: Bool)
}

class PaymentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPaymentTypeName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
}

class TicketPayment: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var protocolDelegate : TicketPaymentViewControllerDelegate?
    
    @IBOutlet weak var txtAmountDue: UITextField!
    @IBOutlet weak var txtTender: UITextField!
    
    
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCredit: UIButton!
    @IBOutlet weak var btnDebit: UIButton!
    @IBOutlet weak var btnGiftCard: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnLoyalty: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblTotalDue: UILabel!
    
    @IBOutlet weak var tblVw: UITableView!
    
    
    var RecordList: [[String:String]] = [[String:String]]()
    
    let date = Date()
    let format = DateFormatter()
    var validate = Validation()
    var progressView = CustomProgressView()
    var selectedIndex = 0
    var pos_transaction_header = PosTransactionHeader()
    var pos_customer = PosCustomers()
    var mvAmountDue: Double = 0
    var mvChange: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtAmountDue.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_net)!, style: .currency)
        self.lblTotalDue.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_net)!, style: .currency)
        //cash
        let x_pos = (btnCash.frame.size.width / 2) - 16
        
        let leftImageView1 = UIImageView()
        leftImageView1.image = UIImage(named: "cash")
        leftImageView1.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnCash.addSubview(leftImageView1)
        
        //credit
        let leftImageView2 = UIImageView()
        leftImageView2.image = UIImage(named: "credit")
        leftImageView2.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnCredit.addSubview(leftImageView2)
        
        
        //debit
        
        let leftImageView3 = UIImageView()
        leftImageView3.image = UIImage(named: "debit")
        leftImageView3.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnDebit.addSubview(leftImageView3)
        
        //gift card
        
        let leftImageView4 = UIImageView()
        leftImageView4.image = UIImage(named: "gift_card")
        leftImageView4.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnGiftCard.addSubview(leftImageView4)
        
        //paycheck
        
        let leftImageView5 = UIImageView()
        leftImageView5.image = UIImage(named: "paycheck")
        leftImageView5.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnCheck.addSubview(leftImageView5)
        
        //loyalty points
        
        let leftImageView6 = UIImageView()
        leftImageView6.image = UIImage(named: "reward")
        leftImageView6.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnLoyalty.addSubview(leftImageView6)


        //cash
        let x_pos2 = (btnBack.frame.size.width / 2) - 16
        
        let leftImageView7 = UIImageView()
        leftImageView7.image = UIImage(named: "back")
        leftImageView7.frame = CGRect(x: x_pos2, y: 5, width: 32, height: 32)
        
        btnBack.addSubview(leftImageView7)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    

    
    //start numpad
    @IBAction func btnNum1(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "1"
    }
    
    @IBAction func btnNum2(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "2"
    }
    
    @IBAction func btnNum3(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "3"
    }
    
    @IBAction func btnNum4(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "4"
    }
    
    @IBAction func btnNum5(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "5"
    }
    
    @IBAction func btnNum6(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "6"
    }
    
    @IBAction func btnNum7(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "7"
    }
    
    @IBAction func btnNum8(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "8"
    }
    
    @IBAction func btnNum9(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "9"
    }
    
    @IBAction func btnNum0(_ sender: Any) {
        self.txtTender.text = self.txtTender.text! + "0"
    }
    
    @IBAction func btnClear(_ sender: Any) {
        self.txtTender.text = ""
    }
    
    @IBAction func btnPoint(_ sender: Any) {
        
        if(self.txtTender.text?.characters.contains(".") != true){
            self.txtTender.text = self.txtTender.text! + "."
        }
        
        
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        if(self.txtTender.text != ""){
            let old_text: String = self.txtTender.text!
            let back_text = old_text.substring(to: old_text.index(before: old_text.endIndex))
            
            self.txtTender.text = back_text
        }
        
        
    }
    
    //end numpad
    
    func OpenModalPaymentReceipt(){
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ModalPaymentReceipt") as! ModalPaymentReceipt
        
        vc.protocolDelegate = self
        vc.pos_transaction_header = self.pos_transaction_header
        vc.change = self.mvChange
        
        self.present(vc, animated: false, completion: nil)
    }
    
    func EnterPayment(payment_type_id:String, payment_type_name:String, tendered:String, amount:String, card_number:String, reference_number: String){
        for var i in 0..<self.RecordList.count {
            
            if(self.RecordList[i]["payment_type_id"] == payment_type_id){
                MyAlert.displayAlert("Ticket Payment", msg: "\(payment_type_name) Already in the list.", controller: self)
                return
            }
            
            i = i + 1
        }
        
        
        if(self.txtTender.text == ""){
            
            let YesNoModal = UIAlertController(title: "Ticket Payment", message: "Are you sure you want to pay the remaining amount with \(payment_type_name)?", preferredStyle: UIAlertControllerStyle.alert)
            
            YesNoModal.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                
                let new_amount = String(describing: self.txtAmountDue.text!.replacingOccurrences(of: "$", with: ""))
                
                self.AddItemToListView(payment_type_id: payment_type_id, payment_type_name: payment_type_name, tendered: new_amount, amount: new_amount, card_number: card_number, reference_number: reference_number)
                
                let response:Dictionary<String, Any> = self.SavePayment()
                
                if(response["success"]as? Bool)!{
                    //self.protocolDelegate?.didOpenTicketPayment(viewController: self, success: true)
                    self.OpenModalPaymentReceipt()
                }else{
                    MyAlert.displayAlertWithDismiss("Ticket Payment", msg: (response["message"] as? String)!, controller: self)
                }
                
                
                
            }))
            
            YesNoModal.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
                //NOTHING
            }))
            
            self.present(YesNoModal, animated: true, completion: nil)
            
        }else{
            self.AddItemToListView(payment_type_id: payment_type_id, payment_type_name: payment_type_name, tendered: tendered, amount: amount, card_number: card_number, reference_number: reference_number)
            
            
            if(mvAmountDue < 1){
                let response:Dictionary<String, Any> = self.SavePayment()
                
                if(response["success"]as? Bool)!{
                    //self.protocolDelegate?.didOpenTicketPayment(viewController: self, success: true)
                    
                    self.OpenModalPaymentReceipt()
                    
                }else{
                    MyAlert.displayAlertWithDismiss("Ticket Payment", msg: (response["message"] as? String)!, controller: self)
                }
            }
            
        }
        
        
    }
    
    @IBAction func btnCash(_ sender: Any) {
        
        var tendered:Double = 0
        
        if(self.txtTender.text! == ""){
            tendered = 0
        }else{
            tendered = Double(String(describing: self.txtTender.text!.replacingOccurrences(of: "$", with: "")))!
        }
        
        let amount_due:Double = Double(String(describing: self.txtAmountDue.text!.replacingOccurrences(of: "$", with: "")))!
        var new_amount: Double = 0
        
        if(tendered > amount_due){
            new_amount = amount_due
        }else if(tendered == 0){
            new_amount = amount_due
        }else{
            new_amount = tendered
        }
        
        self.EnterPayment(payment_type_id: "1", payment_type_name: "Cash", tendered: self.txtTender.text!, amount: String(new_amount), card_number: "", reference_number: "")
    }
    
    @IBAction func btnCredit(_ sender: Any) {
        self.EnterPayment(payment_type_id: "3", payment_type_name: "Credit Card", tendered: self.txtTender.text!, amount: self.txtTender.text!, card_number: "", reference_number: "")
    }
    
    @IBAction func btnDebit(_ sender: Any) {
        self.EnterPayment(payment_type_id: "4", payment_type_name: "Debit Card", tendered: self.txtTender.text!, amount: self.txtTender.text!, card_number: "", reference_number: "")
    }
    
    @IBAction func btnGiftCard(_ sender: Any) {
        self.EnterPayment(payment_type_id: "6", payment_type_name: "Gift Card", tendered: self.txtTender.text!, amount: self.txtTender.text!, card_number: "", reference_number: "")
    }
    
    @IBAction func btnCheck(_ sender: Any) {
        self.EnterPayment(payment_type_id: "2", payment_type_name: "Check", tendered: self.txtTender.text!, amount: self.txtTender.text!, card_number: "", reference_number: "")
    }
    
    @IBAction func btnLoyaltyPoints(_ sender: Any) {
        self.EnterPayment(payment_type_id: "5", payment_type_name: "Loyalty Points", tendered: self.txtTender.text!, amount: self.txtTender.text!, card_number: "", reference_number: "")
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RecordList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblVw.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
        
        
        cell.lblPaymentTypeName.text = RecordList[indexPath.row]["payment_type_name"]!
        
        cell.lblAmount.text = UIUtils.formatCurrency(value: Double(RecordList[indexPath.row]["amount"]!)!, style: .currency)
        
        if(self.RecordList[indexPath.row]["payment_type_id"]! == "1"){
            cell.lblCardNumber.text = "Change: " + UIUtils.formatCurrency(value: Double(RecordList[indexPath.row]["change"]!)!, style: .currency)
        }else{
            cell.lblCardNumber.text = RecordList[indexPath.row]["card_number"]!
        }
        

        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(TicketPayment.btnDelete(sender:)), for: UIControlEvents.touchUpInside)

        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(TicketPayment.LongPressTableViewCell)) //Long function will call when user long press on button.

        
        
        cell.addGestureRecognizer(longGesture)
        
        //UIUtils.formatCurrency(value: self.mvTip, style: .currency)
        
        cell.tag = indexPath.row
        
        return cell
    }
    
    func LongPressTableViewCell() {
        
        print("Long press")
    }
    
    func AddItemToListView(payment_type_id:String, payment_type_name:String, tendered:String, amount:String, card_number:String, reference_number: String){
        for var i in 0..<self.RecordList.count {
            
            if(self.RecordList[i]["payment_type_id"] == payment_type_id){
                MyAlert.displayAlert("Ticket Payment", msg: "\(payment_type_name) Already in the list.", controller: self)
                return
            }
            
            i = i + 1
        }
        
        
//        id
//        line_numer
//        payment_type_id
//        card_numer
//        reference_number
//        amount
        
        let change = Double(tendered)! - Double(amount)!
        
        self.RecordList.append([
            "id":"\(self.pos_transaction_header.id)",
            "line_number":"\(self.RecordList.count + 1)",
            "payment_type_id":"\(payment_type_id)",
            "card_number":"\(card_number)",
            "reference_number":"\(reference_number)",
            "amount":"\(amount)",
            "tendered":"\(tendered)",
            "change":"\(change)",
            "payment_type_name":"\(payment_type_name)"
            ])
        
        
        tblVw.beginUpdates()
        tblVw.insertRows(at: [IndexPath(row: self.RecordList.count-1, section: 0)], with: .automatic)
        tblVw.endUpdates()
        
        self.txtTender.text = ""
        
        //print("current amount due \(String(describing: self.txtAmountDue.text!.replacingOccurrences(of: "$", with: "")))")
        
        let current_amount_due:Double = Double(String(describing: self.txtAmountDue.text!.replacingOccurrences(of: "$", with: "")))!
        let entered_payment_amount:Double = Double(tendered)!
        
        mvChange = mvChange + change
        
        mvAmountDue = Double(current_amount_due - entered_payment_amount)
        
        if(mvAmountDue < 1){
            self.txtAmountDue.text = UIUtils.formatCurrency(value: 0, style: .currency)
        }else{
            self.txtAmountDue.text = UIUtils.formatCurrency(value: mvAmountDue, style: .currency)
        }
        
        
    }
    
    func ReNumber(){
        for var i in 0..<self.RecordList.count {
            self.RecordList[i]["line_number"] = String(i + 1)
            i = i + 1
        }
    }
    
    func btnDelete(sender: UIButton!) {
        let index = sender.tag
        self.selectedIndex = index
        
        let YesNoModal = UIAlertController(title: "Ticket Payment", message: "Are you sure you want to delete this line?", preferredStyle: UIAlertControllerStyle.alert)
        
        YesNoModal.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            
            let current_amount_due:Double = Double(String(describing: self.txtAmountDue.text!.replacingOccurrences(of: "$", with: "")))!
            let entered_payment_amount:Double = Double(self.RecordList[index]["amount"]!)!
            
            if(self.RecordList[index]["payment_type_id"]! == "1"){
                self.mvChange = 0
            }
            
            self.mvAmountDue = Double(current_amount_due + entered_payment_amount)
            
            self.txtAmountDue.text = UIUtils.formatCurrency(value: self.mvAmountDue, style: .currency)
            
            self.RecordList.remove(at: index)
            
            self.ReNumber()
            
            self.tblVw.reloadData()
            
            

            
        }))
        
        YesNoModal.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            //NOTHING
        }))
        
        self.present(YesNoModal, animated: true, completion: nil)
        
    }

    func SavePayment() -> Dictionary<String, Any> {
        
        var response:Dictionary<String, Any> = ["success":false,"message":"Error while saving payment"]
        
        let db_path = MyDefaults.getString("db_path")
        //let db_select = Database()
        
        format.dateFormat = "yyy-MM-dd HH:mm:ss"
        
        let new_date = format.string(from: date as Date)
        
        var cmd = ""
        var success:Bool = false
        var ids = ""
        
        if let queue = FMDatabaseQueue(path: db_path) {
            queue.inTransaction() {
                db, rollback in
                
                cmd = ""
                
                //let rs:FMResultSet? = db?.executeQuery(cmd, withArgumentsIn: nil)
                
                cmd = "update pos_transaction_header set status = 'P' where id = \(self.pos_transaction_header.id)"
                
                success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                
                if !success {
                    rollback?.initialize(to: true)
                    response["message"] = db?.lastErrorMessage()
                    return
                }
                
                var i = 0
                
                
                for cell in self.RecordList {
                
                    cmd = "insert into pos_transaction_payment (id, line_number, payment_type_id, card_number, reference_number, amount, tendered, change) values ("
                
                    cmd = cmd + "'\(self.pos_transaction_header.id)',"
                    cmd = cmd + "'\(i+1)',"
                    cmd = cmd + "'\(cell["payment_type_id"]!)',"
                    cmd = cmd + "'\(cell["card_number"]!)',"
                    cmd = cmd + "'\(cell["reference_number"]!)',"
                    cmd = cmd + "'\(cell["amount"]!)',"
                    cmd = cmd + "'\(cell["tendered"]!)',"
                    cmd = cmd + "'\(cell["change"]!)'"
                
                    cmd = cmd + ")"
                
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    i = i + 1
                }
                
                
                
                // get the trans specialist with service item only
                
                cmd = "select *,count(main_category_id) as total_count, sum(item_net) as total_net from pos_transaction_detail where main_category_id = 1 and id = \(self.pos_transaction_header.id) group by specialist_id"
                
                var rs = FMResultSet()
                
                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                while (rs.next()) {
                    //let main_category_id_count = rs?.string(forColumn: "main_category_id_count") ?? ""
                    // let main_category_id = rs.string(forColumn: "main_category_id") ?? ""
                    let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                    
                    //if(main_category_id == "1"){
                    ids += specialist_id + ","
                    //}
                    
                }
                
                if(ids != ""){
                    ids.remove(at: ids.index(before: ids.endIndex))
                }
                // end
                
                // update the order number without the trans specialist
                
                cmd = "select * from specialist_on_queue where specialist_id not in (\(ids)) order by order_number asc"
                
                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                i = 1
                
                while (rs.next()) {
                    let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                    
                    
                    success = (db?.executeUpdate("update specialist_on_queue set order_number = \(i) where specialist_id = \(specialist_id)", withArgumentsIn:nil))!
                    
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    i = i + 1
                }
                // end
                
                // update the order number of the trans specialist
                //                cmd = "select * from specialist_on_que where specialist_id in (\(ids))"
                //
                //                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                //
                //                while (rs.next()) {
                //                    let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                //
                //
                //                    success = (db?.executeUpdate("update specialist_on_queue set order_number = \(i), status = 'A', transaction_id = -1 where specialist_id = \(specialist_id)", withArgumentsIn:nil))!
                //
                //                    if !success {
                //                        rollback?.initialize(to: true)
                //                        return
                //                    }
                //
                //                    i = i + 1
                //                }
                
                
                
                cmd = "select *,count(main_category_id) as total_count, sum(item_net) as total_net from pos_transaction_detail where main_category_id = 1 and id = \(self.pos_transaction_header.id) group by specialist_id"
                
                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                while (rs.next()) {
                    
                    let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                    let total_count = rs.string(forColumn: "total_count") ?? ""
                    let total_net = rs.string(forColumn: "total_net") ?? ""
                    
                    success = (db?.executeUpdate("update specialist_on_queue set order_number = \(i), status = 'A', transaction_id = -1, total_count = total_count + \(total_count), total_amount = total_amount + \(total_net) where specialist_id = \(specialist_id)", withArgumentsIn:nil))!
                    
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    
                }
                //
                
                
                cmd = "select * from pos_customers where id = \(self.pos_customer.id)"
                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                while (rs.next()) {
                    
                    let first_visit_date = rs.string(forColumn: "first_visit_date") ?? ""
                    //                    let last_visit_date = rs.string(forColumn: "last_visit_date") ?? ""
                    //                    let visit_count = rs.string(forColumn: "visit_count") ?? "0"
                    //                    let total_spent = rs.string(forColumn: "total_spent") ?? "0"
                    
                    cmd = "update pos_customers set "
                    cmd = cmd + "last_visit_date = '\(new_date)',"
                    cmd = cmd + "total_spent = total_spent + \(self.pos_transaction_header.total_net),"
                    
                    if(first_visit_date == ""){
                        cmd = cmd + "first_visit_date = '\(new_date)',"
                    }
                    
                    cmd = cmd + "visit_count = visit_count + 1 "
                    
                    cmd = cmd + "where id = \(self.pos_customer.id) "
                    
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                    
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    
                }
                
                
                return response = ["success":true,"message":"Ticket successfully paid."]
                
                
                
                
                
                
            } // end queue.inTransaction()
        } // end que if let queue
        
        return response
    } //end quick cash function

}




extension TicketPayment:ModalPaymentReceiptViewControllerDelegate {
    func didOpenModalPaymentReceipt(viewController vc: ModalPaymentReceipt, success: Bool)
    {
        vc.dismiss(animated: true, completion: nil)
        
        if(success){
            self.protocolDelegate?.didOpenTicketPayment(viewController: self, success: true)
        }
        
        
    }
}
