//
//  SelectTicketItem.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/15/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit

protocol SelectTicketItemViewControllerDelegate {
    func didOpenSelectTicketItem(viewController vc: SelectTicketItem, success: Bool, method: String, quantity:String, price:String, discount_id: String, is_percent : String, item_discount:String, item_remarks:String, employee_id:String, employee_name:String)
}


class TransferSpecialistCell: UICollectionViewCell{
    
    
    @IBOutlet weak var lblSpecialistName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    
    
    @IBOutlet weak var imgSpecialistPicture: UIImageView!
    
}


class SelectTicketItem: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnTransfer: UIButton!
    @IBOutlet weak var btnVoidItem: UIButton!
    @IBOutlet weak var btnItemDiscount: UIButton!
    @IBOutlet weak var btnQuantity: UIButton!
    
    @IBOutlet weak var conVw: UIView!

    
    
    @IBOutlet weak var colVw: UICollectionView!
    
    @IBOutlet weak var txtRequest: UITextView!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var vwNumpad: UIView!
    
    @IBOutlet weak var btnNumBack: UIButton!
    @IBOutlet weak var btnNumUpdate: UIButton!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    var SpecialistList: [[String:String]] = [[String:String]]()
    
    var progressView = CustomProgressView()
    
    let date = Date()
    let format = DateFormatter()
    
    var item_remarks = ""
    //var employee = Employee()
    var specialist_id = ""
    var specialist_name = ""
    var discount_id = ""
    
        var protocolDelegate : SelectTicketItemViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtRequest.text = self.item_remarks
        
        //request
        let x_pos = (btnRequest.frame.size.width / 2) - 16
        
        let leftImageView1 = UIImageView()
        leftImageView1.image = UIImage(named: "idea")
        leftImageView1.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnRequest.addSubview(leftImageView1)
        
        //transfer
        let leftImageView2 = UIImageView()
        leftImageView2.image = UIImage(named: "move")
        leftImageView2.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnTransfer.addSubview(leftImageView2)
        
        
        //void item
        
        let leftImageView3 = UIImageView()
        leftImageView3.image = UIImage(named: "void")
        leftImageView3.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnVoidItem.addSubview(leftImageView3)
        
        //item discount
        
        let leftImageView4 = UIImageView()
        leftImageView4.image = UIImage(named: "discount")
        leftImageView4.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        if(self.discount_id == "-1"){
            btnItemDiscount.setTitle("Add Discount", for: .normal)
        }else{
            btnItemDiscount.setTitle("Remove Disc.", for: .normal)
        }
        
        btnItemDiscount.addSubview(leftImageView4)
        
        //cancel
        
        let leftImageView5 = UIImageView()
        leftImageView5.image = UIImage(named: "cancel")
        leftImageView5.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnCancel.addSubview(leftImageView5)
        
        //ok
        
        let leftImageView6 = UIImageView()
        leftImageView6.image = UIImage(named: "ok")
        leftImageView6.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnOk.addSubview(leftImageView6)
        
        //quantity
        
        let leftImageView7 = UIImageView()
        leftImageView7.image = UIImage(named: "add")
        leftImageView7.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnQuantity.addSubview(leftImageView7)
        
        //num back
        
        let x_pos_back = (btnNumBack.frame.size.width / 2) - 16
        
        let leftImageView8 = UIImageView()
        leftImageView8.image = UIImage(named: "back")
        leftImageView8.frame = CGRect(x: x_pos_back, y: 5, width: 32, height: 32)
        
        btnNumBack.addSubview(leftImageView8)
        
        //update
        
        let x_pos_update = (btnNumUpdate.frame.size.width / 2) - 16
        
        let leftImageView9 = UIImageView()
        leftImageView9.image = UIImage(named: "update")
        leftImageView9.frame = CGRect(x: x_pos_update, y: 5, width: 32, height: 32)
        
        btnNumUpdate.addSubview(leftImageView9)

    }
    
    func get_specialist_on_queue(specialist_id: String){
        
        if (self.SpecialistList.count > 0) {
            self.SpecialistList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select spq.*, e.first_name, e.last_name, e.profile_image from specialist_on_queue spq left join employee e on (e.id = spq.specialist_id) where spq.specialist_id <> \(specialist_id) order by order_number asc"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        
        
        while (rs.next()) {
            
            
            let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
            let first_name = rs.string(forColumn: "first_name") ?? ""
            let last_name = rs.string(forColumn: "last_name") ?? ""
            let profile_image = rs.string(forColumn: "profile_image") ?? ""
            let order_number = rs.string(forColumn: "order_number") ?? ""
            let total_count = rs.string(forColumn: "total_count") ?? ""
            let total_amount = rs.string(forColumn: "total_amount") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            
            if(status == "A"){
                
                let obj = [
                    "specialist_id":"\(specialist_id)",
                    "first_name":"\(first_name)",
                    "last_name":"\(last_name)",
                    "profile_image":"\(profile_image)",
                    "order_number":"\(order_number)",
                    "total_count":"\(total_count)",
                    "total_amount":"\(total_amount)",
                    "status":"\(status)",
                ]
                SpecialistList.append(obj)
                
            }
            
            
        }
        
        
        
        self.colVw.reloadData()
        db.closeDB()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SpecialistList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TransferSpecialistCell", for: indexPath) as! TransferSpecialistCell
        
        let row = indexPath.row
        let obj = SpecialistList[row] as NSDictionary
        
        let specialist_name = SpecialistList[row]["first_name"]! + " " + SpecialistList[row]["last_name"]!
        let amount = obj["total_amount"] as! String
        let count = obj["total_count"] as! String
        let order_number = obj["order_number"] as! String
        let picture = obj["profile_image"] as! String
        
        //            cell.vwCell.layer.borderWidth = 1
        //            cell.vwCell.layer.borderColor = UIUtils.colorWithHexString(hex: "#EDEDED").cgColor
        
        cell.lblSpecialistName.text = specialist_name
        cell.lblAmount.text = UIUtils.formatCurrency(value: Double(amount)!, style: .currency)
        cell.lblCount.text = "Count = " + count
        cell.lblNo.text = "# " + order_number
        cell.tag = row
        
        
        
        let dataDecoded = NSData(base64Encoded: picture, options: NSData.Base64DecodingOptions(rawValue: 0))
        let decodedimage = UIImage(data: dataDecoded! as Data)
        
        //dataDecoded
        
        if(decodedimage == nil){
            cell.imgSpecialistPicture.image = UIImage(named: "user")
        }else{
            cell.imgSpecialistPicture.image = decodedimage
        }
        
        
        //cell.imgSpecialistPicture.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        print("main category id = \(selected_main_category_id) item name = \(self.ItemList[indexPath.row]["name"]!) price = \(self.ItemList[indexPath.row]["price"]!)")
//        
//        self.protocolDelegate?.didOpenTicketItems(viewController: self, main_category_id: self.selected_main_category_id, item_id: self.ItemList[indexPath.row]["id"]!, price: self.ItemList[indexPath.row]["price"]!, item_name: self.ItemList[indexPath.row]["name"]!)
        
        
        print("row number \(indexPath.row) specialist_name = \(SpecialistList[indexPath.row]["first_name"]!) \(SpecialistList[indexPath.row]["last_name"]!)")
        
        
        self.protocolDelegate?.didOpenSelectTicketItem(viewController: self, success: true, method: "edit", quantity: "", price: "", discount_id: "", is_percent: "", item_discount: "", item_remarks: "", employee_id: self.SpecialistList[indexPath.row]["specialist_id"]!, employee_name: SpecialistList[indexPath.row]["first_name"]! + " " + SpecialistList[indexPath.row]["last_name"]!)
        //self.OpenTicket(index: indexPath.row, is_fast_sale: false)
        
    }


    //start numpad
    @IBAction func btnNum1(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "1"
    }
    
    @IBAction func btnNum2(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "2"
    }
    
    @IBAction func btnNum3(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "3"
    }
    
    @IBAction func btnNum4(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "4"
    }
    
    @IBAction func btnNum5(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "5"
    }
    
    @IBAction func btnNum6(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "6"
    }
    
    @IBAction func btnNum7(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "7"
    }
    
    @IBAction func btnNum8(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "8"
    }
    
    @IBAction func btnNum9(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "9"
    }
    
    @IBAction func btnNum0(_ sender: Any) {
        self.txtPassword.text = self.txtPassword.text! + "0"
    }
    
    @IBAction func btnClear(_ sender: Any) {
        self.txtPassword.text = ""
    }
    
    
    @IBAction func btnUpdate(_ sender: Any) {
        self.protocolDelegate?.didOpenSelectTicketItem(viewController: self, success: true, method: "edit", quantity: self.txtPassword.text!, price:"", discount_id: "", is_percent: "", item_discount:"", item_remarks: "", employee_id:"", employee_name:"")
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        if(self.txtPassword.text != ""){
            let old_text: String = self.txtPassword.text!
            let back_text = old_text.substring(to: old_text.index(before: old_text.endIndex))
            
            self.txtPassword.text = back_text
        }
        
        
    }
    
    //end numpad
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func btnRequest(_ sender: Any) {
        
        self.conVw.isHidden = true
        
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddRequest") as! AddRequest
        
        vc.protocolDelegate = self
        
        self.present(vc, animated: false, completion: nil)
    }

    @IBAction func btnItemDiscount(_ sender: Any) {
        
//        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ModalAddDiscount") as! ModalAddDiscount
//        
//        vc.protocolDelegate = self
//        
//        self.present(vc, animated: false, completion: nil)
        
        
        if(self.discount_id == "-1"){
            self.removeSubviews()
            
            self.txtRequest.isHidden = true
            self.btnCancel.isHidden = true
            self.btnOk.isHidden = true
            self.vwNumpad.isHidden = true
            self.colVw.isHidden = true
            conVw.isHidden = false
            
            
            let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectDiscount") as! SelectDiscount
            vc.protocolDelegate = self
            
            vc.modalPresentationStyle = .overCurrentContext
            addChildViewController(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
            conVw.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }else{
            self.protocolDelegate?.didOpenSelectTicketItem(viewController: self, success: true, method: "edit", quantity: "", price: "", discount_id: "-1", is_percent: "", item_discount: "", item_remarks: "", employee_id: "", employee_name: "")
        }

        
        
    }
    
    // Remove all child subviews before addsubviews
    func removeSubviews() {
        for view in self.conVw.subviews{
            view.removeFromSuperview()
        }
    }
    
    @IBAction func btnQuantity(_ sender: Any) {
        self.txtRequest.isHidden = true
        self.btnCancel.isHidden = true
        self.btnOk.isHidden = true
        self.vwNumpad.isHidden = false
        self.colVw.isHidden = true
        self.conVw.isHidden = true
    }
    
    
    @IBAction func btnVoidItem(_ sender: Any) {
        self.protocolDelegate?.didOpenSelectTicketItem(viewController: self, success: true, method: "delete", quantity:"", price:"", discount_id: "", is_percent: "", item_discount:"", item_remarks:"", employee_id:"", employee_name:"")
    }
    
    @IBAction func btnTransfer(_ sender: Any) {
        self.txtRequest.isHidden = true
        self.btnCancel.isHidden = true
        self.btnOk.isHidden = true
        self.vwNumpad.isHidden = true
        self.colVw.isHidden = false
        self.conVw.isHidden = true
        self.get_specialist_on_queue(specialist_id: self.specialist_id)
        
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.txtRequest.isHidden = true
        self.btnCancel.isHidden = true
        self.btnOk.isHidden = true
        self.vwNumpad.isHidden = true
        self.colVw.isHidden = true
        self.conVw.isHidden = true
    }
    
    @IBAction func btnOk(_ sender: Any) {
        self.txtRequest.isHidden = true
        self.btnCancel.isHidden = true
        self.btnOk.isHidden = true
        self.vwNumpad.isHidden = true
        self.colVw.isHidden = true
        self.conVw.isHidden = true
        self.protocolDelegate?.didOpenSelectTicketItem(viewController: self, success: true, method: "edit", quantity:"", price:"", discount_id: "", is_percent: "", item_discount:"", item_remarks: self.txtRequest.text!, employee_id:"", employee_name:"")
        
    }
    
    

}




extension SelectTicketItem:ModalAddDiscountViewControllerDelegate {
    func didOpenModalAddDiscount(viewController vc: ModalAddDiscount, success: Bool)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        if success
        {
            vc.dismiss(animated: true, completion: nil)
            
            self.protocolDelegate?.didOpenSelectTicketItem(viewController: self, success: true, method: "add_discount", quantity: "", price: "", discount_id: "", is_percent: "", item_discount: "", item_remarks: "", employee_id: "", employee_name: "")

            
            
            
        }
        
    }
}

extension SelectTicketItem:SelectDiscountViewControllerDelegate {
    func didOpenSelectDiscount(viewController vc: SelectDiscount, success: Bool, discount_id: String, value : String, is_percent : String)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)
            
            
            self.protocolDelegate?.didOpenSelectTicketItem(viewController: self, success: true, method: "edit", quantity: "", price: "", discount_id: discount_id, is_percent: is_percent, item_discount: value, item_remarks: "", employee_id: "", employee_name: "")
            
            
        }
        
    }
}

extension SelectTicketItem:AddRequestViewControllerDelegate {
    func didOpenAddRequest(viewController vc: AddRequest, button_str: String)
    {
        vc.dismiss(animated: true, completion: nil)
        
        if(button_str == "add"){
            self.txtRequest.isHidden = false
            self.btnCancel.isHidden = false
            self.btnOk.isHidden = false
            self.colVw.isHidden = true

        }else{
            self.txtRequest.isHidden = true
            self.btnCancel.isHidden = true
            self.btnOk.isHidden = true
            self.colVw.isHidden = true
        }
        
    }
}


