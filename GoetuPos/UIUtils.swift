//
//  UIUtils.swift
//  Go3Reservation
//
//  Created by Luigi Guevarra on 5/3/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit

class UIUtils {
    
    class func setTextFieldPadding(_ textField: UITextField) {
        let passwordPadding = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.height))
        textField.leftView = passwordPadding
        textField.leftViewMode = UITextFieldViewMode.always
        textField.layer.borderColor = UIColor.gray.cgColor
    }
    
    class func setTextFieldBorderColor(_ textField: UITextField, hex: String) {
        textField.layer.borderColor = colorWithHexString(hex: hex).cgColor
    }
    
    class func setViewBorderColor(_ myView: UIView, hex: String) {
        myView.layer.borderColor = colorWithHexString(hex: hex).cgColor
    }
    
    class func colorWithHexString (hex: String) -> UIColor {
        var cString:String = hex
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    class func formatCurrency(value: Double, style: NumberFormatter.Style) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = style
        formatter.maximumFractionDigits = 2;
        formatter.locale = Locale(identifier: Locale.current.identifier)
        let result = formatter.string(from: value as NSNumber);
        return result!;
    }
    
    
    class func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        var mask = "XXX-XXX-XXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask.characters {
            if index == cleanPhoneNumber.endIndex {
                break
            }
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    
}
