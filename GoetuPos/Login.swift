//
//  Login.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/26/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit
import Foundation



class Login: UIViewController {

    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    
    @IBOutlet weak var lblVersionNumber: UILabel!
    
    var progress = CustomProgressView()
    let db: Database = Database()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtUserName.text = "admin"
        txtPassword.text = "admin"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    @IBAction func btnSignIn(_ sender: Any) {
        var username: String!
        var password: String!
        
        username = txtUserName.text
        password = txtPassword.text
        
        
        
        //db.create_additional_tables() //create if additional tables if not exist
        
        
//        var sqlCmd = "SELECT module_code FROM user_module LIMIT 1"
//        var rs = db.getResults(sqlCmd)
//        if  (rs.next() == false)
//        {
//            db.insert_system_module_default_printer()
//        }
//        
//        sqlCmd = "SELECT id FROM discount_type LIMIT 1"
//        
//        rs = db.getResults(sqlCmd)
//        
//        if  (rs.next() == true){
//            //do nothing
//        }else{
//            //insert a default value
//            var cmd_insert = " INSERT INTO  discount_type (name, description, create_date, create_by, status) VALUES ('Discount by Flat Amount', 'ItemAmount', '01-01-2017', 'DEFAULT', 'A')"
//            db.executeNonQuery(cmd_insert)
//            
//            cmd_insert = " INSERT INTO  discount_type (name, description, create_date, create_by, status) VALUES ('Discount by Percentage', 'ItemPercentage', '01-01-2017', 'DEFAULT', 'A')"
//            db.executeNonQuery(cmd_insert)
//        }
//        
//        db.closeDB()
        
        
        
        progress.showActivityIndicator(self.view, message: "Logging in")
        
        if (username == "" || password == ""){
            self.progress.hideActivityIndicator(self.view)
            MyAlert.displayAlertWithDismiss("Login", msg: "Enter your username/password", controller: self)
        }else{
            //let db: Database = Database()
            let cmd = "SELECT u.*, e.first_name, e.last_name,ep.notes as employee_code  FROM USER u left join employee e on (u.id = e.user_id) left join employee_positions ep on (ep.id = e.employee_position_id) WHERE u.username= '\(username!)' AND u.password='\(password!)'"
            
            let rs = db.getResults(cmd)
            if rs.next() == true{
                
                let is_owner = rs.string(forColumn: "is_owner")
                let first_name = rs.string(forColumn: "first_name")
                let last_name = rs.string(forColumn: "last_name")
                let employee_code = rs.string(forColumn: "employee_code")
                //let user_type_id = rs.string(forColumn: "user_type_id")
                let user_id = rs.string(forColumn: "id")
                
                MyDefaults.saveString("username", value: username)
                MyDefaults.saveString("is_owner", value: is_owner)
                MyDefaults.saveString("first_name", value: first_name)
                MyDefaults.saveString("last_name", value: last_name)
                MyDefaults.saveString("employee_code", value: employee_code)
                MyDefaults.saveString("user_id", value: user_id)
                
                let date = Date()
                let format = DateFormatter()
                format.dateFormat = "MM/dd/yyyy"
                
                let current_date = format.string(from: date as Date)
                
                var install_date = db.getDBSetting("install_date")
                
//                let r_username = db.getDBSetting("reservation_user")
//                let r_password = db.getDBSetting("reservation_password")
                
                if install_date == ""{
                    db.setDBSetting("install_date", setting_value: current_date)
                    install_date = current_date
                }
                
                let start = install_date
                let end = current_date
                
                let startDate:Date = format.date(from: start)!
                let endDate:Date = format.date(from: end)!
                
                let components = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
                
                var remaining_days = 30
                
                remaining_days = remaining_days - components.day!
                
                if remaining_days == 0
                {
                    progress.hideActivityIndicator(self.view)
                    MyAlert.displayAlertWithDismiss("GOETU", msg: "Please contact your pos provider. Your Nails POS has expired.", controller: self)
                    return
                }
                
                
                let refreshAlert = UIAlertController(title: "GOETU", message: "You only have \(remaining_days) day(s) to evaluate our Nails POS.", preferredStyle: UIAlertControllerStyle.alert)
                
                MyDefaults.saveString("session_id", value: "")
                MyDefaults.saveString("reservation_name", value: "")
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    if (is_owner == "1"){
                        //AppDelegate.setAccessRights(["ALL"])
                    }else{
//                        cmd = "SELECT um.* FROM user_type_module_access utma LEFT JOIN user_module um ON utma.user_module_id=um.id WHERE user_type_id=\(user_type_id!)"
//                        let rsAccess = db.getResults(cmd)
//                        var accessRights = [String]()
//                        while (rsAccess.next()) {
//                            let module_code = rsAccess.string(forColumn: "module_code")
//
//                            accessRights.append(module_code!)
//                        }

                    }
                    
                    
                    
                    
                    
                    self.db.closeDB()
                    

                    
//                    Alamofire.request(AppConstant.endpoint_url, parameters: login_params)
//                        .responseJSON { response in
//                            
//                            if (response.result.error != nil) {
//                                print(response.result.error!.localizedDescription)
//                                global.saveString("session_id", value: "")
//                                global.saveString("reservation_name", value: "")
//                                self.progress.hideActivityIndicator(self.view)
//                                self.performSegue(withIdentifier: "loginSegue", sender: self)
//                            } else {
//                                print("api response:\n\(response.result.value!)")
//                                var json = JSON(response.result.value!)
//                                
//                                let respcode = json["respcode"].stringValue
//                                if (respcode != "0000") {
//                                    let message = json["respmsg"].stringValue
//                                    print(message)
//                                    global.saveString("session_id", value: "")
//                                    global.saveString("reservation_name", value: "")
//                                    self.progress.hideActivityIndicator(self.view)
//                                    self.performSegue(withIdentifier: "loginSegue", sender: self)
//                                } else {
//                                    let data = json["respmsg"]
//                                    let permissions = data["Permissions"].stringValue
//                                    let session_id = data["SessionID"].stringValue
//                                    let fname = data["fname"].stringValue
//                                    let lname = data["lname"].stringValue
//                                    
//                                    global.saveString("session_id", value: session_id)
//                                    global.saveString("reservation_name", value: r_username)
//                                    self.progress.hideActivityIndicator(self.view)
//                                    self.performSegue(withIdentifier: "loginSegue", sender: self)
//                                }
//                            }
//                    } //login
                    
                }))
                

                
              //  self.present(refreshAlert, animated: true)
                
                
                MyDefaults.getTax()
                
                self.progress.hideActivityIndicator(self.view)
                self.performSegue(withIdentifier: "toMain", sender: self)
                
            }else{
                self.progress.hideActivityIndicator(self.view)
                MyAlert.displayAlertWithDismiss("Login", msg: "Invalid username/password", controller: self)
            }
            self.db.closeDB()
        }
        //        progress.hideActivityIndicator(self.view)
    }

    
    

    
    @IBAction func btnForgotPassword(_ sender: Any) {
        
    }
    

}
