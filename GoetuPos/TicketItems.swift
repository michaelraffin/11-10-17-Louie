//
//  TicketItems.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/14/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit
import Foundation

protocol TicketItemsViewControllerDelegate {
    func didOpenTicketItems(viewController vc: TicketItems, main_category_id: String, item_id: String, price: String, item_name: String)
}

class CategoryListCell: UITableViewCell {
    
    @IBOutlet weak var lblCategoryName: UILabel!

}

class ColVwCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblItemName: UILabel!
    
}


class TicketItems: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    var protocolDelegate : TicketItemsViewControllerDelegate?

    @IBOutlet weak var btnService: UIButton!
    @IBOutlet weak var btnProducts: UIButton!
    @IBOutlet weak var btnPackages: UIButton!
    @IBOutlet weak var btnGiftCards: UIButton!
    
    @IBOutlet weak var colVw: UICollectionView!
    @IBOutlet weak var tblVw: UITableView!
    
    @IBOutlet weak var colVwLeading: NSLayoutConstraint!
    
    var not_selected_color: UIColor!
    var selected_color: UIColor!
    
    var categoryList: [[String:String]] = [[String:String]]()
    var ItemList: [[String:String]] = [[String:String]]()
    
    var progressView = CustomProgressView()
    
    let date = Date()
    let format = DateFormatter()
    
    var selected_category_row = 0
    var selected_main_category_id = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.not_selected_color = btnProducts.backgroundColor
        self.selected_color = btnService.backgroundColor

        self.getCategories()
        self.getServices(category_id: Int(self.categoryList[0]["id"]!)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    

    @IBAction func btnServices(_ sender: Any) {
        btnService.backgroundColor = self.selected_color
        btnProducts.backgroundColor = self.not_selected_color
        btnPackages.backgroundColor = self.not_selected_color
        btnGiftCards.backgroundColor = self.not_selected_color
        
        colVwLeading.constant = 154
        tblVw.isHidden = false
        self.getServices(category_id: Int(self.categoryList[0]["id"]!)!)
        selected_main_category_id = "1"
    }
    
    @IBAction func btnProducts(_ sender: Any) {
        btnService.backgroundColor = self.not_selected_color
        btnProducts.backgroundColor = self.selected_color
        btnPackages.backgroundColor = self.not_selected_color
        btnGiftCards.backgroundColor = self.not_selected_color
        
        colVwLeading.constant = 0
        tblVw.isHidden = true
        self.getProducts()
        selected_main_category_id = "2"
    }
    
    @IBAction func btnPackages(_ sender: Any) {
        btnService.backgroundColor = self.not_selected_color
        btnProducts.backgroundColor = self.not_selected_color
        btnPackages.backgroundColor = self.selected_color
        btnGiftCards.backgroundColor = self.not_selected_color
        
        colVwLeading.constant = 0
        tblVw.isHidden = true
        self.getPackages()
        selected_main_category_id = "3"
    }
    
    @IBAction func btnGiftCards(_ sender: Any) {
        btnService.backgroundColor = self.not_selected_color
        btnProducts.backgroundColor = self.not_selected_color
        btnPackages.backgroundColor = self.not_selected_color
        btnGiftCards.backgroundColor = self.selected_color
        
        colVwLeading.constant = 0
        tblVw.isHidden = true
        self.getGiftCards()
        selected_main_category_id = "4"
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.categoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        

        
        
        
        let cell = self.tblVw.dequeueReusableCell(withIdentifier: "SelectCategoryCell", for: indexPath) as! CategoryListCell
        
        cell.lblCategoryName.text = self.categoryList[indexPath.row]["name"]!

        return cell
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        
            let id:Int = Int(self.categoryList[indexPath.row]["id"]!)!
            self.getServices(category_id:id)
            selected_category_row = indexPath.row
        
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ItemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColVwCell", for: indexPath) as! ColVwCell
        
        //        let backgroundView = UIView()
        //        backgroundView.backgroundColor = UIUtils.colorWithHexString(hex: "#ffffff")
        //        cell.selectedBackgroundView = backgroundView
        //        if(cell.responds(to: #selector(setter: UIView.layoutMargins))){
        //            cell.layoutMargins = UIEdgeInsets.zero;
        //        }
        
        let row = indexPath.row
        let obj = ItemList[row] as NSDictionary
        
        //let id = obj["id"] as! String
        let name = obj["name"] as! String
        let price = obj["price"] as! String
        //

        cell.lblPrice.text = UIUtils.formatCurrency(value: Double(price)!, style: .currency)
        cell.lblItemName.text = name
        

        
//        cell.btnDelete.tag = row
//        cell.btnDelete.addTarget(self, action: #selector(Categories.btnDeleteAction(sender:)), for: UIControlEvents.touchUpInside)
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("main category id = \(selected_main_category_id) item name = \(self.ItemList[indexPath.row]["name"]!) price = \(self.ItemList[indexPath.row]["price"]!)")
        
        self.protocolDelegate?.didOpenTicketItems(viewController: self, main_category_id: self.selected_main_category_id, item_id: self.ItemList[indexPath.row]["id"]!, price: self.ItemList[indexPath.row]["price"]!, item_name: self.ItemList[indexPath.row]["name"]!)
        
        
    }
    
    func getGiftCards(){
        
        if (self.ItemList.count > 0) {
            self.ItemList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from gift_cards where status = 'A'"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            let id = rs.string(forColumn: "id") ?? ""
            let name = rs.string(forColumn: "name") ?? ""
            let notes = rs.string(forColumn: "notes") ?? ""
            let price = rs.string(forColumn: "price") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            let create_date = rs.string(forColumn: "create_date") ?? ""
            let create_by = rs.string(forColumn: "create_by") ?? ""
            let update_date = rs.string(forColumn: "update_date") ?? ""
            let update_by = rs.string(forColumn: "update_by") ?? ""
            
            
            let obj = [
                "id":"\(id)",
                "name":"\(name)",
                "notes":"\(notes)",
                "price":"\(price)",
                "status":"\(status)",
                "create_date":"\(create_date)",
                "create_by":"\(create_by)",
                "update_date":"\(update_date)",
                "update_by":"\(update_by)",
            ]
            self.ItemList.append(obj)
            
            
            
            
        }
        
        
        self.colVw.reloadData()
        //    self.colVwSpecialist.reloadData()
        //    self.colVwInService.reloadData()
        db.closeDB()
    }
    
    func getPackages(){
        
        if (self.ItemList.count > 0) {
            self.ItemList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from packages where status = 'A'"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            let id = rs.string(forColumn: "id") ?? ""
            let name = rs.string(forColumn: "name") ?? ""
            let notes = rs.string(forColumn: "notes") ?? ""
            let price = rs.string(forColumn: "price") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            let create_date = rs.string(forColumn: "create_date") ?? ""
            let create_by = rs.string(forColumn: "create_by") ?? ""
            let update_date = rs.string(forColumn: "update_date") ?? ""
            let update_by = rs.string(forColumn: "update_by") ?? ""
            
            
            let obj = [
                "id":"\(id)",
                "name":"\(name)",
                "notes":"\(notes)",
                "price":"\(price)",
                "status":"\(status)",
                "create_date":"\(create_date)",
                "create_by":"\(create_by)",
                "update_date":"\(update_date)",
                "update_by":"\(update_by)",
            ]
            self.ItemList.append(obj)
            
            
            
            
        }
        
        
        self.colVw.reloadData()
        //    self.colVwSpecialist.reloadData()
        //    self.colVwInService.reloadData()
        db.closeDB()
    }
    
    func getProducts(){
        
        if (self.ItemList.count > 0) {
            self.ItemList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from products where status = 'A'"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            let id = rs.string(forColumn: "id") ?? ""
            let name = rs.string(forColumn: "name") ?? ""
            let notes = rs.string(forColumn: "notes") ?? ""
            let price = rs.string(forColumn: "price") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            let create_date = rs.string(forColumn: "create_date") ?? ""
            let create_by = rs.string(forColumn: "create_by") ?? ""
            let update_date = rs.string(forColumn: "update_date") ?? ""
            let update_by = rs.string(forColumn: "update_by") ?? ""
            
            
            let obj = [
                "id":"\(id)",
                "name":"\(name)",
                "notes":"\(notes)",
                "price":"\(price)",
                "status":"\(status)",
                "create_date":"\(create_date)",
                "create_by":"\(create_by)",
                "update_date":"\(update_date)",
                "update_by":"\(update_by)",
            ]
            self.ItemList.append(obj)
            
            
            
            
        }
        
        
        self.colVw.reloadData()
        //    self.colVwSpecialist.reloadData()
        //    self.colVwInService.reloadData()
        db.closeDB()
    }
    
    
    func getServices(category_id:Int){
        
        if (self.ItemList.count > 0) {
            self.ItemList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from services where status = 'A' and service_type_id = \(category_id)"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            let id = rs.string(forColumn: "id") ?? ""
            let name = rs.string(forColumn: "name") ?? ""
            let notes = rs.string(forColumn: "notes") ?? ""
            let price = rs.string(forColumn: "price") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            let create_date = rs.string(forColumn: "create_date") ?? ""
            let create_by = rs.string(forColumn: "create_by") ?? ""
            let update_date = rs.string(forColumn: "update_date") ?? ""
            let update_by = rs.string(forColumn: "update_by") ?? ""
            
            
            let obj = [
                "id":"\(id)",
                "name":"\(name)",
                "notes":"\(notes)",
                "price":"\(price)",
                "status":"\(status)",
                "create_date":"\(create_date)",
                "create_by":"\(create_by)",
                "update_date":"\(update_date)",
                "update_by":"\(update_by)",
            ]
            self.ItemList.append(obj)
            
            
            
            
        }
        
        
        self.colVw.reloadData()
        //    self.colVwSpecialist.reloadData()
        //    self.colVwInService.reloadData()
        db.closeDB()
    }
    
    
    func getCategories(){
        
        
        if (self.categoryList.count > 0) {
            self.categoryList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from categories where status = 'A'"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            let id = rs.string(forColumn: "id") ?? ""
            let name = rs.string(forColumn: "name") ?? ""
            let notes = rs.string(forColumn: "notes") ?? ""
            let price = rs.string(forColumn: "price") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            let create_date = rs.string(forColumn: "create_date") ?? ""
            let create_by = rs.string(forColumn: "create_by") ?? ""
            let update_date = rs.string(forColumn: "update_date") ?? ""
            let update_by = rs.string(forColumn: "update_by") ?? ""
            
                
                let obj = [
                    "id":"\(id)",
                    "name":"\(name)",
                    "notes":"\(notes)",
                    "price":"\(price)",
                    "status":"\(status)",
                    "create_date":"\(create_date)",
                    "create_by":"\(create_by)",
                    "update_date":"\(update_date)",
                    "update_by":"\(update_by)",
                ]
                self.categoryList.append(obj)
                

            
            
        }
        
        
        self.tblVw.reloadData()
    //    self.colVwSpecialist.reloadData()
    //    self.colVwInService.reloadData()
        db.closeDB()
    
    }


}
