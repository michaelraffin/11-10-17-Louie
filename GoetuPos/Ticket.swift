//
//  Ticket.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/14/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit
import Foundation

protocol TicketViewControllerDelegate {
    func didOpenTicket(viewController vc: Ticket, success: Bool)
}

class PosTransactionHeader: NSObject {

    
    
    var id = ""
    var doc_code = ""
    var trans_code = ""
    var type_code = ""
    var site_code = ""
    var status = ""
    var terminal_id = "-1"
    var reference_number = ""
    var remarks = ""
    var discount_id = ""
    var customer_id = "-1"
    var specialist_id = ""
    var total_discount = "0"
    var total_item_discount = "0"
    var total_tax = "0"
    var total_tax_exempt = "0"
    var total_tip = "0"
    var tip_is_percent = "0"
    var total_gross = "0"
    var total_net = "0"
    var create_date = ""
    var create_by = ""
    var update_date = ""
    var update_by = ""
    var cancel_date = ""
    var cancel_by = ""
    
    var discount_is_percent = "0"
    var discount_value = "0"
    
    var total_products = "0"
    var total_services = "0"
    var total_gift_cards = "0"
    var total_packages = "0"
    
}

class PosTransactionDetail: NSObject {
    
    
    
    var id = ""
    var line_number = ""
    var specialist_id = ""
    var commision_id = ""
    var main_category_id = ""
    var item_id = ""
    var quantity = "0"
    var price = "0"
    var discount_id = "-1"
    var discount_is_percent = ""
    var discount_value = ""
    var item_discount = "0"
    var item_gross = "0"
    var item_net = "0"
    var item_remarks = ""
    var item_name = ""
    
    
    var first_name = ""
    var last_name = ""

}

class PosTransactionPayment: NSObject {
    
    
    
    var id = ""
    var line_numer = ""
    var payment_type_id = ""
    var card_numer = ""
    var reference_number = ""
    var amount = "0"
    
    
}

class PosCustomers: NSObject {
    
    
    var id = "-1"
    var first_name = ""
    var last_name = ""
    var status = ""
    var notes = ""
    var gender = ""
    var civil_status = ""
    var address = ""
    var state_id = "-1"
    var city_id = "-1"
    var zip_code = ""
    var member_id = ""
    var mobile_number = ""
    var email_address = ""
    var birthdate = ""
    var default_kit_id = "-1"
    var first_visit_date = ""
    var last_visit_date = ""
    var visit_count = "0"
    var total_spent = "0"
    var loyalty_points = "0"
    var create_date = ""
    var create_by = ""
    var update_date = ""
    var update_by = ""
    
    
}

class InventoryHeader: NSObject {
    
    
    var site_code = ""
    var main_category_id = ""
    var item_id = ""
    var quantity = "0"
    var update_date = ""
    
    
}

class Employee: NSObject {
    
    
    var id = ""
    var user_id = ""
    var commision_id = ""
    var last_name = ""
    var first_name = ""
    var middle_name = ""
    var mobile_number = ""
    var email_address = ""
    var birth_date = ""
    var address = ""
    var state = ""
    var city = ""
    var zip = ""
    var gender = ""
    var profile_image = ""
    var create_date = ""
    var create_by = ""
    var update_date = ""
    var update_by = ""
    var status = ""
    var clock_pin = ""
    var employee_position_id = ""
    
}

class Category: NSObject {
    var id = ""
    var name = ""
    var notes = ""
    var status = ""
    var create_date = ""
    var create_by = ""
    var update_date = ""
    var update_by = ""
}

class Service: NSObject {
    var id = ""
    var main_category_id = ""
    var name = ""
    var notes = ""
    var service_type_id = ""
    var minutes = ""
    var price = ""
    var status = ""
    var create_date = ""
    var create_by = ""
    var update_date = ""
    var update_by = ""
}

class Product: NSObject {
    var id = ""
    var main_category_id = ""
    var name = ""
    var notes = ""
    var service_type_id = ""
    var minutes = ""
    var cost = ""
    var price = ""
    var status = ""
    var create_date = ""
    var create_by = ""
    var update_date = ""
    var update_by = ""
}

class Package: NSObject {
    var id = ""
    var main_category_id = ""
    var name = ""
    var notes = ""
    var service_type_id = ""
    var minutes = ""
    var cost = ""
    var price = ""
    var status = ""
    var create_date = ""
    var create_by = ""
    var update_date = ""
    var update_by = ""
}

class SpecialistOnQueue: NSObject {
    var specialist_id = ""
    var order_number = ""
    var total_count = ""
    var total_amount = ""
    var status = ""
}

class TicketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTicketQuantity: UILabel!
    @IBOutlet weak var lblTicketItemName: UILabel!
    @IBOutlet weak var lblTicketSpecialist: UILabel!
    @IBOutlet weak var lblTicketItemPrice: UILabel!
    
    
    
}

class Ticket: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lblTicketHeader: UILabel!
    @IBOutlet weak var lblTicketDate: UILabel!
    @IBOutlet weak var txtCustomer: UITextField!
    
    @IBOutlet weak var lblMemberId: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblBirthDate: UILabel!
    @IBOutlet weak var lblDefaultKit: UILabel!
    
    @IBOutlet weak var lblFirstVisit: UILabel!
    @IBOutlet weak var lblLastVisit: UILabel!
    @IBOutlet weak var lblVisitCount: UILabel!
    @IBOutlet weak var lblTotalSpend: UILabel!
    @IBOutlet weak var lblTotalPoints: UILabel!

    @IBOutlet weak var tblVw: UITableView!
    
    @IBOutlet weak var lblRetail: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblGiftCards: UILabel!
    @IBOutlet weak var lblPackages: UILabel!
    
    
    @IBOutlet weak var lblbTipsLabel: UILabel!
    @IBOutlet weak var lblTips: UILabel!
    
    @IBOutlet weak var lblDiscountLabel: UILabel!
    @IBOutlet weak var lblDiscounts: UILabel!
    
    
    @IBOutlet weak var lblTaxLabel: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var btnVoid: UIButton!
    @IBOutlet weak var btnTax: UIButton!
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnQuickCash: UIButton!
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnTips: UIButton!
    @IBOutlet weak var btnDiscounts: UIButton!
    @IBOutlet weak var btnCustomers: UIButton!
    @IBOutlet weak var btnPayments: UIButton!
    
    @IBOutlet weak var btnExit: UIButton!
    
    @IBOutlet weak var conVw: UIView!
    
    var protocolDelegate : TicketViewControllerDelegate?
    
    var pos_transaction_header = PosTransactionHeader()
    var pos_transaction_detail = [PosTransactionDetail]()
    var pos_transaction_payment = PosTransactionPayment()
    var pos_customer = PosCustomers()
    var employee = Employee()
    var service = Service()
    var product = Product()
    var package = Package()
    var specialist_on_queue = SpecialistOnQueue()
    
    var mvGross : Double = 0
    var mvNet : Double = 0
    var mvPayment : Double = 0
    var mvItemDiscount : Double = 0
    var mvDiscount : Double = 0
    var mvTip : Double = 0
    var mvTax : Double = 0
    var mvTaxExempt : Double = 0
    var mvCost : Double = 0
    
    var mvIsTaxExempt : Bool = false
    
    var mvCash : Double = 0
    var mvCheque : Double = 0
    var mvCreditCard : Double = 0
    var mvDebitCard : Double = 0
    var mvLoyalty : Double = 0
    
    var mvCashTendered : Double = 0
    var mvOthersTendered : Double = 0
    
    var mvServicesTotal : Double = 0
    var mvProductsTotal : Double = 0
    var mvPackagesTotal : Double = 0
    var mvGiftCardsTotal : Double = 0
    
    var validate = Validation()
    var progressView = CustomProgressView()
    var RecordList: [[String:String]] = [[String:String]]()
    var selectedIndex = 0
//    var EditingItemId = ""
//    var EditingMainCategoryId = ""
    var isEdit:Bool = false
    
    let date = Date()
    let format = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.InitializeTicket()
        
        //void
        let x_pos = (btnVoid.frame.size.width / 2) - 16
        
        let leftImageView1 = UIImageView()
        leftImageView1.image = UIImage(named: "void")
        leftImageView1.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnVoid.addSubview(leftImageView1)
        
        //cash_drawer
        let leftImageView2 = UIImageView()
        leftImageView2.image = UIImage(named: "tax")
        leftImageView2.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnTax.addSubview(leftImageView2)
        
        
        //print
        
        let leftImageView3 = UIImageView()
        leftImageView3.image = UIImage(named: "print")
        leftImageView3.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnPrint.addSubview(leftImageView3)
        
        //done
        
        let leftImageView4 = UIImageView()
        leftImageView4.image = UIImage(named: "done")
        leftImageView4.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnDone.addSubview(leftImageView4)
        
        //quick_cash
        
        let leftImageView5 = UIImageView()
        leftImageView5.image = UIImage(named: "cash")
        leftImageView5.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnQuickCash.addSubview(leftImageView5)
        
        //tax
        
        let leftImageView6 = UIImageView()
        leftImageView6.image = UIImage(named: "details")
        leftImageView6.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnMenu.addSubview(leftImageView6)
        
        //tips
        
        let leftImageView7 = UIImageView()
        leftImageView7.image = UIImage(named: "tips")
        leftImageView7.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnTips.addSubview(leftImageView7)
        
        //discount
        
        let leftImageView8 = UIImageView()
        leftImageView8.image = UIImage(named: "discount")
        leftImageView8.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnDiscounts.addSubview(leftImageView8)
        
        //customer
        
        let leftImageView9 = UIImageView()
        leftImageView9.image = UIImage(named: "customer")
        leftImageView9.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnCustomers.addSubview(leftImageView9)
        
        //payment
        
        let leftImageView10 = UIImageView()
        leftImageView10.image = UIImage(named: "payment")
        leftImageView10.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnPayments.addSubview(leftImageView10)
        
        //exit
        
        let leftImageView11 = UIImageView()
        leftImageView11.image = UIImage(named: "cancel")
        leftImageView11.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnExit.addSubview(leftImageView11)
        
        self.showTicketItems()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func AddItemToListView(main_category_id:String, item_id:String, price:String, item_name:String){
        for var i in 0..<self.RecordList.count {
            
            if(self.RecordList[i]["main_category_id"] == main_category_id && self.RecordList[i]["item_id"] == item_id){
                MyAlert.displayAlert("Ticket", msg: "Item Already in the list.", controller: self)
                return
            }
            
            i = i + 1
        }
        
//        if(self.EditingMainCategoryId == "" && self.EditingItemId == ""){
            
//            id
//            line_number
//            specialist_id
//            commision_id
//            main_category_id
//            item_id
//            quantity
//            price
//            item_discount
//            item_gross
//            item_net
//            item_remarks
        
        
            
            self.RecordList.append([
                "id":"\(self.pos_transaction_header.id)",
                "line_number":"\(self.RecordList.count + 1)",
                "specialist_id":"\(self.employee.id)",
                "commision_id":"-1",
                "main_category_id":"\(main_category_id)",
                "item_id":"\(item_id)",
                "quantity":"1",
                "price":"\(price)",
                "discount_id":"-1",
                "discount_is_percent":"",
                "discount_value":"",
                "item_discount":"0",
                "item_gross":"\(price)",
                "item_net":"\(price)",
                "item_remarks":"",
                "item_name":"\(item_name)",
                "specialist_name":"\(self.employee.first_name + " " + self.employee.last_name)"
                ])
            
            
            tblVw.beginUpdates()
            tblVw.insertRows(at: [IndexPath(row: self.RecordList.count-1, section: 0)], with: .automatic)
            tblVw.endUpdates()
        
            self.ComputeTotal(main_category_id: main_category_id, is_less: false, gross: Double(price)!, payment: 0, item_discount: 0, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
        
        //}
//        else{
//            //let total_sms:Int = Int(self.RecordList[selectedIndex]["quantity"]!)! * Int(self.RecordList[selectedIndex]["package_quantity"]!)!
//            
//            
//            self.RecordList[selectedIndex]["item_name"] = item_name
//            
//            self.RecordList[selectedIndex]["quantity"] = txtQuantity.text!
//            
//            self.RecordList[selectedIndex]["package_quantity"] = txtPackageSms.text!
//            
//            self.RecordList[selectedIndex]["sms_package_id"] = txtSmsPackageID.text!
//            
//            self.tblVw.reloadData()
//            
//            self.ClearItemEditor()
//        }
    }
    
    func ClearItemEditor(){
//        self.EditingItemId = ""
//        self.EditingMainCategoryId = ""
        self.selectedIndex = 0

    }
    
    func ComputeTotal(main_category_id : String, is_less : Bool, gross : Double, payment : Double, item_discount : Double, discount : Double, tip : Double, cost : Double, cash : Double, cheque : Double, credit_card : Double, debit_card : Double, loyalty : Double, cash_tendered : Double, others_tendered : Double){
        
        if(!is_less){
            mvGross = mvGross + gross
            mvPayment = mvPayment + payment
            mvItemDiscount = mvItemDiscount + item_discount
            mvDiscount = mvDiscount + discount
            //mvTip = mvTip + tip
            mvCost = mvCost + cost
            mvCash = mvCash + cash
            mvCheque = mvCheque + cheque
            mvCreditCard = mvCreditCard + credit_card
            mvDebitCard = mvDebitCard + debit_card
            mvLoyalty = mvLoyalty + loyalty
            mvCashTendered = mvCashTendered + cash_tendered
            mvOthersTendered = mvOthersTendered + others_tendered
            
            if(main_category_id == "1"){
                mvServicesTotal = mvServicesTotal + (gross - item_discount)
            }else if(main_category_id == "2"){
                mvProductsTotal = mvProductsTotal + (gross - item_discount)
            }else if(main_category_id == "3"){
                mvPackagesTotal = mvPackagesTotal + (gross - item_discount)
            }else if(main_category_id == "4"){
                mvGiftCardsTotal = mvGiftCardsTotal + (gross - item_discount)
            }
            
            
        }else{
            mvGross = mvGross - gross
            mvPayment = mvPayment - payment
            mvItemDiscount = mvItemDiscount - item_discount
            mvDiscount = mvDiscount - discount
            //mvTip = mvTip - tip
            mvCost = mvCost - cost
            mvCash = mvCash - cash
            mvCheque = mvCheque - cheque
            mvCreditCard = mvCreditCard - credit_card
            mvDebitCard = mvDebitCard - debit_card
            mvLoyalty = mvLoyalty - loyalty
            mvCashTendered = mvCashTendered - cash_tendered
            mvOthersTendered = mvOthersTendered - others_tendered
            
            if(main_category_id == "1"){
                mvServicesTotal = mvServicesTotal - (gross - item_discount)
            }else if(main_category_id == "2"){
                mvProductsTotal = mvProductsTotal - (gross - item_discount)
            }else if(main_category_id == "3"){
                mvPackagesTotal = mvPackagesTotal - (gross - item_discount)
            }else if(main_category_id == "4"){
                mvGiftCardsTotal = mvGiftCardsTotal - (gross - item_discount)
            }
            
        }
        
        if(self.pos_transaction_header.discount_is_percent == "1"){
            mvDiscount = (Double(self.pos_transaction_header.discount_value)! * 0.01) * (mvGross - mvItemDiscount)
        }else{
            mvDiscount = Double(self.pos_transaction_header.discount_value)!
        }
        
        mvNet = mvGross - (mvItemDiscount + mvDiscount)
        
        //compute tax
        mvTax = (Double(MyDefaults.getString("tax"))! * 0.01) * mvNet
        
        if(mvIsTaxExempt){
            mvTaxExempt = mvTax
            mvTax = 0
            mvNet = mvNet - mvTaxExempt
        }else{
            mvTaxExempt = 0
            //mvNet = mvNet - mvTax
        }
        
        //
        
        
        
        if(self.pos_transaction_header.tip_is_percent == "1"){
            mvTip = (Double(self.pos_transaction_header.total_tip)! * 0.01) * mvNet
        }else{
            mvTip = Double(self.pos_transaction_header.total_tip)!
        }
        
        
        
        mvNet = mvNet + mvTip
        
        self.lblTips.text = UIUtils.formatCurrency(value: self.mvTip, style: .currency)
        
        if(mvIsTaxExempt){
            self.lblTax.text = UIUtils.formatCurrency(value: self.mvTaxExempt, style: .currency)
        }else{
            self.lblTax.text = UIUtils.formatCurrency(value: self.mvTax, style: .currency)
        }
        
        self.lblSubtotal.text = UIUtils.formatCurrency(value: self.mvGross - self.mvItemDiscount, style: .currency)
        self.lblDiscounts.text = UIUtils.formatCurrency(value: self.mvDiscount, style: .currency)
        self.lblTotalAmount.text = UIUtils.formatCurrency(value: self.mvNet, style: .currency)
        
        self.lblService.text = UIUtils.formatCurrency(value: self.mvServicesTotal, style: .currency)
        self.lblRetail.text = UIUtils.formatCurrency(value: self.mvProductsTotal, style: .currency)
        self.lblPackages.text = UIUtils.formatCurrency(value: self.mvPackagesTotal, style: .currency)
        self.lblGiftCards.text = UIUtils.formatCurrency(value: self.mvGiftCardsTotal, style: .currency)
        
    }
    
    func EditItemToListView(quantity:String, price:String, discount_id:String, is_percent:String, item_discount:String, item_remarks:String, employee_id:String, employee_name:String){
    
    var new_quantity = ""
    var new_price = ""
    //var new_item_discount = ""
    var new_item_remarks = ""
    var new_employee_id = ""
    var new_employee_name = ""
    var new_discount_id = ""
    var new_is_percent = ""
    var new_discount_value = ""
        
        
    self.ComputeTotal(main_category_id: self.RecordList[selectedIndex]["main_category_id"]!, is_less: true, gross: Double(self.RecordList[selectedIndex]["price"]!)! * Double(self.RecordList[selectedIndex]["quantity"]!)!, payment: 0, item_discount: Double(self.RecordList[selectedIndex]["item_discount"]!)!, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
        
        
    if(quantity == ""){
        new_quantity = self.RecordList[selectedIndex]["quantity"]!
    }else{
        new_quantity = quantity
    }
    
    if(price == ""){
        new_price = self.RecordList[selectedIndex]["price"]!
    }else{
        new_price = price
    }
    
//    if(item_discount == ""){
//        new_item_discount = self.RecordList[selectedIndex]["item_discount"]!
//    }else{
//        new_item_discount = item_discount
//    }
    
    if(is_percent == ""){
        new_is_percent = self.RecordList[selectedIndex]["discount_is_percent"]!
    }else{
        new_is_percent = is_percent
    }
        
    if(item_discount == ""){
        new_discount_value = self.RecordList[selectedIndex]["discount_value"]!
    }else{
        new_discount_value = item_discount
    }
        
    if(discount_id == ""){
        new_discount_id = self.RecordList[selectedIndex]["discount_id"]!
    }else if(discount_id == "-1"){
        new_discount_id = ""
        new_is_percent = ""
        new_discount_value = ""
    }else{
        new_discount_id = discount_id
    }
        
    if(item_remarks == ""){
        new_item_remarks = self.RecordList[selectedIndex]["item_remarks"]!
    }else{
        new_item_remarks = item_remarks
    }
    
    if(employee_id == ""){
        new_employee_id = self.RecordList[selectedIndex]["specialist_id"]!
    }else{
        new_employee_id = employee_id
    }
    
    if(employee_name == ""){
        new_employee_name = self.RecordList[selectedIndex]["specialist_name"]!
    }else{
        new_employee_name = employee_name
    }
    
    
        let item_gross = Double(new_quantity)! * Double(new_price)!
        
        var computed_discount : Double = 0
        
        if(new_is_percent == "1"){
            computed_discount = item_gross * (0.01 * Double(new_discount_value)!)
        }else if(new_is_percent == "0"){
            computed_discount = Double(new_discount_value)!
        }
        
        
        let item_net = item_gross - computed_discount
    
    
        self.RecordList[selectedIndex]["quantity"] = new_quantity
    
        self.RecordList[selectedIndex]["price"] = new_price
    
        self.RecordList[selectedIndex]["discount_id"] = new_discount_id
        
        self.RecordList[selectedIndex]["discount_is_percent"] = new_is_percent
        
        self.RecordList[selectedIndex]["discount_value"] = new_discount_value
        
        self.RecordList[selectedIndex]["item_discount"] = String(computed_discount)
    
        self.RecordList[selectedIndex]["item_gross"] = String(item_gross)
    
        self.RecordList[selectedIndex]["item_net"] = String(item_net)
    
        self.RecordList[selectedIndex]["item_remarks"] = new_item_remarks
    
        self.RecordList[selectedIndex]["specialist_id"] = new_employee_id
    
        self.RecordList[selectedIndex]["specialist_name"] = new_employee_name
    
        self.tblVw.reloadData()
    
        self.ComputeTotal(main_category_id: self.RecordList[selectedIndex]["main_category_id"]!, is_less: false, gross: Double(self.RecordList[selectedIndex]["price"]!)! * Double(self.RecordList[selectedIndex]["quantity"]!)!, payment: 0, item_discount: Double(self.RecordList[selectedIndex]["item_discount"]!)!, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
        
        self.ClearItemEditor()
    
        self.removeSubviews()
        self.showTicketItems()
    
    }
    
    func DeleteItemToListView(){
        
        let YesNoModal = UIAlertController(title: "Ticket", message: "Are you sure you want to void this item?", preferredStyle: UIAlertControllerStyle.alert)
        
        YesNoModal.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            
            
            self.ComputeTotal(main_category_id: self.RecordList[self.selectedIndex]["main_category_id"]!, is_less: true, gross: Double(self.RecordList[self.selectedIndex]["price"]!)! * Double(self.RecordList[self.selectedIndex]["quantity"]!)!, payment: 0, item_discount: Double(self.RecordList[self.selectedIndex]["item_discount"]!)!, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
            
            self.RecordList.remove(at: self.selectedIndex)
            
            self.ReNumber()
            
            self.tblVw.reloadData()
            

            
            //if(self.RecordList.count == 0){
                self.removeSubviews()
                self.showTicketItems()
            //}
            
        }))
        
        YesNoModal.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            //NOTHING
        }))
        
        self.present(YesNoModal, animated: true, completion: nil)
    }
    
    func ReNumber(){
        for var i in 0..<self.RecordList.count {
            self.RecordList[i]["line_number"] = String(i + 1)
            i = i + 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RecordList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblVw.dequeueReusableCell(withIdentifier: "TicketTableViewCell", for: indexPath) as! TicketTableViewCell
        
        //let total_sms:Int = Int(RecordList[indexPath.row]["quantity"]!)! * Int(RecordList[indexPath.row]["package_quantity"]!)!
        
        cell.lblTicketQuantity.text = RecordList[indexPath.row]["quantity"]!
        cell.lblTicketSpecialist.text = RecordList[indexPath.row]["specialist_name"]!
        cell.lblTicketItemName.text = RecordList[indexPath.row]["item_name"]!
        cell.lblTicketItemPrice.text = UIUtils.formatCurrency(value: Double(RecordList[indexPath.row]["item_net"]!)!, style: .currency)
        
        //UIUtils.formatCurrency(value: self.mvTip, style: .currency)
        
        cell.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        
//        let id:Int = Int(self.categoryList[indexPath.row]["id"]!)!
//        self.getServices(category_id:id)
//        selected_category_row = indexPath.row
        
        self.removeSubviews()
        
        conVw.isHidden = false
        
        selectedIndex = indexPath.row
        
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SelectTicketItem") as! SelectTicketItem
        vc.protocolDelegate = self
        vc.item_remarks = self.RecordList[indexPath.row]["item_remarks"]!
        //vc.employee.id =
        vc.specialist_id = self.RecordList[indexPath.row]["specialist_id"]!
        vc.specialist_name = self.RecordList[indexPath.row]["specialist_name"]!
        vc.discount_id = self.RecordList[indexPath.row]["discount_id"]!
        
        
        vc.modalPresentationStyle = .overCurrentContext
        addChildViewController(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
        conVw.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
        
    }

    
    @IBAction func btnVoid(_ sender: Any) {
        self.removeSubviews()
        
        conVw.isHidden = false
        
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SelectVoidReason") as! SelectVoidReason
        
        vc.protocolDelegate = self
        
        vc.modalPresentationStyle = .overCurrentContext
        addChildViewController(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
        conVw.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }

    @IBAction func btnTax(_ sender: Any) {
        //self.showTicketItems()
        
        if(mvIsTaxExempt){
            mvIsTaxExempt = false
            self.btnTax.setTitle("Remove Tax", for: .normal)
            self.lblTaxLabel.text = "Tax"
        }else{
            mvIsTaxExempt = true
            self.btnTax.setTitle("Tax", for: .normal)
            self.lblTaxLabel.text = "Tax (Exempt)"
        }
        
        mvTip = 0
        mvDiscount = 0
        mvTax = 0
        mvTaxExempt = 0
        
        self.ComputeTotal(main_category_id: "0", is_less: true, gross: 0, payment: 0, item_discount: 0, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
        
    }
    
    @IBAction func btnPrint(_ sender: Any) {


        

        
    }
    
    func VoidTransaction(id: String, void_reason_id : String) -> Dictionary<String, Any> {
        let db_path = MyDefaults.getString("db_path")
        //let db_select = Database()
        
        var response:Dictionary<String, Any> = ["success":false,"message":"Error while voiding transaction"]
        
        var cmd = ""
        var success:Bool = false
        
        format.dateFormat = "yyy-MM-dd HH:mm:ss"
        
        let new_date = format.string(from: date as Date)
        let cancel_by = "1"
        
        if let queue = FMDatabaseQueue(path: db_path) {
            queue.inTransaction() {
                db, rollback in
                
                cmd = ""
                
                cmd = "update pos_transaction_header set status = 'V', cancel_by = \(cancel_by), cancel_date = '\(new_date)', void_reason_id = \(void_reason_id) where id = \(id)"
                
                success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                
                if !success {
                    rollback?.initialize(to: true)
                    response["message"] = db?.lastErrorMessage()
                    return
                }
                
                success = (db?.executeUpdate("update specialist_on_queue set status = 'A', transaction_id = -1 where specialist_id = \(self.employee.id)", withArgumentsIn:nil))!
                
                if !success {
                    // Need to rollback here
                    rollback?.initialize(to: true)
                    response["message"] = db?.lastErrorMessage()
                    return
                }
                
                return response = ["success":true,"message":"Ticket successfully voided."]
                
            }
        }
        
        return response
    }
    
    
    func SaveTransaction() -> Dictionary<String, Any> {
        let db_path = MyDefaults.getString("db_path")
        //let db_select = Database()
        
        var response:Dictionary<String, Any> = ["success":false,"message":"Error while saving transaction"]
        
        format.dateFormat = "yyy-MM-dd HH:mm:ss"
        
        let new_date = format.string(from: date as Date)
        
        //let doc_code = ""
        let trans_code = "REL"
        let type_code = "POS"
        let site_code = "001"
        
//        var total_tax:Double = 0
//        var total_tax_exempt:Double = 0
        
        let status = "S"
        let terminal_id = "-1"
        let reference_number = ""
        let remarks = ""
        let discount_id = self.pos_transaction_header.discount_id
        let customer_id = self.pos_transaction_header.customer_id
        let specialist_id = self.employee.id
        let total_discount = mvDiscount
        let total_item_discount = mvItemDiscount
        let total_tax = mvTax
        let total_tax_exempt = mvTaxExempt
        let total_tip = self.pos_transaction_header.total_tip
        let tip_is_percent = self.pos_transaction_header.tip_is_percent
        let total_gross = mvGross
        let total_net = mvNet
        let create_date = new_date
        let create_by = MyDefaults.getString("user_id")!
        let update_date = new_date
        let update_by = MyDefaults.getString("user_id")!
        let cancel_date = ""
        let cancel_by = ""
        
        var cmd = ""
        var counter_value = ""
        var success:Bool = false
        var last_insert_id:Int = 0
        
        
        if let queue = FMDatabaseQueue(path: db_path) {
            queue.inTransaction() {
                db, rollback in
                
                if(self.pos_transaction_header.id == ""){
                    
                    cmd = "select value+1 as value from counter where code = '\(site_code)\(trans_code)\(type_code)'"
                    
                    
                    
                    let rs:FMResultSet? = db?.executeQuery(cmd, withArgumentsIn: nil)
                    
                    
                    //let rs = db_select.getResults(cmd)
                    
                    while (rs?.next())! {
                        counter_value = rs?.string(forColumn: "value") ?? ""
                    }
                    
                    if(counter_value == ""){
                        
                        success = (db?.executeUpdate("INSERT INTO counter (code, value) values ('\(site_code)\(trans_code)\(type_code)', '1')", withArgumentsIn:nil))!
                        
                        if !success {
                            rollback?.initialize(to: true)
                            response["message"] = db?.lastErrorMessage()
                            return
                            
                        }else{
                            counter_value = "1"
                        }
                        
                    }
                    
                    
                    cmd = "insert into pos_transaction_header (doc_code, trans_code, type_code, site_code, status, terminal_id, reference_number, remarks, discount_id, customer_id, specialist_id, total_discount, total_item_discount, total_tax, total_tax_exempt, total_tip, tip_is_percent, total_gross, total_net, create_date, create_by, update_date, update_by) values ("
                    cmd = cmd + "'\(counter_value)',"
                    cmd = cmd + "'\(trans_code)',"
                    cmd = cmd + "'\(type_code)',"
                    cmd = cmd + "'\(site_code)',"
                    cmd = cmd + "'\(status)',"
                    cmd = cmd + "'\(terminal_id)',"
                    cmd = cmd + "'\(reference_number)',"
                    cmd = cmd + "'\(remarks)',"
                    cmd = cmd + "'\(discount_id)',"
                    cmd = cmd + "'\(customer_id)',"
                    cmd = cmd + "'\(specialist_id)',"
                    cmd = cmd + "'\(total_discount)',"
                    cmd = cmd + "'\(total_item_discount)',"
                    cmd = cmd + "'\(total_tax)',"
                    cmd = cmd + "'\(total_tax_exempt)',"
                    cmd = cmd + "'\(total_tip)',"
                    cmd = cmd + "'\(tip_is_percent)',"
                    cmd = cmd + "'\(total_gross)',"
                    cmd = cmd + "'\(total_net)',"
                    cmd = cmd + "'\(create_date)',"
                    cmd = cmd + "'\(create_by)',"
                    cmd = cmd + "'\(update_date)',"
                    cmd = cmd + "'\(update_by)'"
                    
                    cmd = cmd + ")"
                    
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                    
                    if !success {
                        // Need to rollback here
                        
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }else{
                        last_insert_id = Int((db?.lastInsertRowId())!)
                        self.pos_transaction_header.id = String(last_insert_id)
                    }
                    
                    var i = 0
                    
                    
                    for cell in self.RecordList {
                        //cell as! TicketTableViewCell
                        
                        success = (db?.executeUpdate("insert into pos_transaction_detail(id, line_number, specialist_id, commision_id, main_category_id, item_id, quantity, price, discount_id, item_discount, item_gross, item_net, item_remarks) values (\(last_insert_id), \(i+1), \(cell["specialist_id"]!), \(cell["commision_id"]!), \(cell["main_category_id"]!), \(cell["item_id"]!), \(cell["quantity"]!), \(cell["price"]!), \(cell["discount_id"]!), \(cell["item_discount"]!), \(cell["item_gross"]!), \(cell["item_net"]!), '\(cell["item_remarks"]!)')", withArgumentsIn:nil))!
                        
                        if !success {
                            // Need to rollback here
                            rollback?.initialize(to: true)
                            response["message"] = db?.lastErrorMessage()
                            return
                        }
                        i = i + 1
                    }
                    
                    success = (db?.executeUpdate("update specialist_on_queue set status = 'I', transaction_id = \(last_insert_id) where specialist_id = \(self.employee.id)", withArgumentsIn:nil))!
                    
                    if !success {
                        // Need to rollback here
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                   // response["brian"] = 4.5
                    //response["mike"] = "hello"
                    
                    self.pos_transaction_header.total_discount = String(self.mvDiscount)
                    self.pos_transaction_header.total_item_discount = String(self.mvItemDiscount)
                    self.pos_transaction_header.total_tax = String(self.mvTax)
                    self.pos_transaction_header.total_tax_exempt = String(self.mvTaxExempt)
                    self.pos_transaction_header.total_tip = String(self.mvTip)
                    self.pos_transaction_header.total_gross = String(self.mvGross)
                    self.pos_transaction_header.total_net = String(self.mvNet)
                    
                    
                    return response = ["success":true,"message":"Ticket successfully saved."]
                    
                    //return response
                    
                }else{
                    cmd = "update pos_transaction_header set "
                    cmd = cmd + "status = '\(status)',"
                    cmd = cmd + "terminal_id = '\(terminal_id)',"
                    cmd = cmd + "reference_number = '\(reference_number)',"
                    cmd = cmd + "remarks = '\(remarks)',"
                    cmd = cmd + "discount_id = '\(discount_id)',"
                    cmd = cmd + "customer_id = '\(customer_id)',"
                    cmd = cmd + "specialist_id = '\(specialist_id)',"
                    cmd = cmd + "total_discount = '\(total_discount)',"
                    cmd = cmd + "total_item_discount = '\(total_item_discount)',"
                    cmd = cmd + "total_tax = '\(total_tax)',"
                    cmd = cmd + "total_tax_exempt = '\(total_tax_exempt)',"
                    cmd = cmd + "total_tip = '\(total_tip)',"
                    cmd = cmd + "tip_is_percent = '\(tip_is_percent)',"
                    cmd = cmd + "total_gross = '\(total_gross)',"
                    cmd = cmd + "total_net = '\(total_net)',"
                    //cmd = cmd + "create_date = '\(create_date)',"
                    //cmd = cmd + "create_by = '\(create_by)',"
                    cmd = cmd + "update_date = '\(update_date)',"
                    cmd = cmd + "update_by = '\(update_by)',"
                    cmd = cmd + "cancel_date = '\(cancel_date)',"
                    cmd = cmd + "cancel_by = '\(cancel_by)'"
                    
                    cmd = cmd + "where id = \(self.pos_transaction_header.id) "
                    
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                    
                    if !success {
                        // Need to rollback here
                        
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    //delete all details first
                    cmd = "delete from pos_transaction_detail where id = \(self.pos_transaction_header.id)"
                    
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                    
                    if !success {
                        // Need to rollback here
                        
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    var i = 0
                    
                    for cell in self.RecordList {
                        //cell as! TicketTableViewCell
                        
                        success = (db?.executeUpdate("insert into pos_transaction_detail(id, line_number, specialist_id, commision_id, main_category_id, item_id, quantity, price, discount_id, item_discount, item_gross, item_net, item_remarks) values (\(self.pos_transaction_header.id), \(i+1), \(cell["specialist_id"]!), \(cell["commision_id"]!), \(cell["main_category_id"]!), \(cell["item_id"]!), \(cell["quantity"]!), \(cell["price"]!), \(cell["discount_id"]!), \(cell["item_discount"]!), \(cell["item_gross"]!), \(cell["item_net"]!), '\(cell["item_remarks"]!)')", withArgumentsIn:nil))!
                        
                        if !success {
                            // Need to rollback here
                            rollback?.initialize(to: true)
                            response["message"] = db?.lastErrorMessage()
                            return
                        }
                        i = i + 1
                    }
                    
                    success = (db?.executeUpdate("update specialist_on_queue set status = 'I', transaction_id = \(self.pos_transaction_header.id) where specialist_id = \(self.employee.id)", withArgumentsIn:nil))!
                    
                    if !success {
                        // Need to rollback here
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    
                    self.pos_transaction_header.total_discount = String(self.mvDiscount)
                    self.pos_transaction_header.total_item_discount = String(self.mvItemDiscount)
                    self.pos_transaction_header.total_tax = String(self.mvTax)
                    self.pos_transaction_header.total_tax_exempt = String(self.mvTaxExempt)
                    self.pos_transaction_header.total_tip = String(self.mvTip)
                    self.pos_transaction_header.total_gross = String(self.mvGross)
                    self.pos_transaction_header.total_net = String(self.mvNet)
                    
                    return response = ["success":true,"message":"Ticket successfully updated."]
                    
                    
                    
                }                // end if else if
                
                
                
                
                
            } // end transac sql
        } // queue = FMDatabaseQueue

        //response["message"] = "Error while saving transaction"
        
        return response
        
    }//end save function
    
    
    @IBAction func btnDone(_ sender: Any) {
        
        let response:Dictionary<String, Any> = self.SaveTransaction()
        
        let alert = UIAlertController(title: "Ticket", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            //self.dismiss(animated: false, completion: nil)
            if(response["success"]as? Bool)!{
                self.protocolDelegate?.didOpenTicket(viewController: self, success: true)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func ReSaveTransaction() -> Bool {
        let response:Dictionary<String, Any> = self.SaveTransaction()
        let result:Bool = Bool((response["success"] as? Bool)!)
        
        if(!result){
            
            MyAlert.displayAlertWithDismiss("Ticket", msg: (response["message"] as? String)!, controller: self)
            return false
        }else{
            return true
        }
        
    }
    
    @IBAction func btnQuickCash(_ sender: Any) {
        if(self.ReSaveTransaction()){
            let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ModalQuickCash") as! ModalQuickCash
            
            vc.protocolDelegate = self
            vc.pos_transaction_header = self.pos_transaction_header
            vc.pos_customer = self.pos_customer
            //vc.pos_transaction_detail = self.pos_transaction_detail
            //vc.pos_transaction_payment = self.pos_transaction_payment
            
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func btnMenu(_ sender: Any) {
        self.showTicketItems()
    }
    
    @IBAction func btnTips(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ModalAddTips") as! ModalAddTips
//        
//        vc.protocolDelegate = self
//        
//        self.present(vc, animated: false, completion: nil)
        
        
        
        
        //
        if(Double(self.pos_transaction_header.total_tip)! > 0){
            self.btnTips.setTitle("Tips", for: .normal)
            
//            self.pos_transaction_header.discount_id = ""
//            self.pos_transaction_header.discount_value = "0"
//            self.pos_transaction_header.discount_is_percent = "0"
            
            self.pos_transaction_header.total_tip = "0"
            self.pos_transaction_header.tip_is_percent = "0"
            
            mvTip = 0
            mvDiscount = 0
            mvTax = 0
            mvTaxExempt = 0
            self.ComputeTotal(main_category_id: "0", is_less: true, gross: 0, payment: 0, item_discount: 0, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
            
            
            
        }else{
            self.removeSubviews()
            
            conVw.isHidden = false
            
            let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EnterTips") as! EnterTips
            vc.protocolDelegate = self
            //vc.is_ticket = true
            
            vc.modalPresentationStyle = .overCurrentContext
            addChildViewController(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
            conVw.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
            
            
            //self.btnDiscounts.setTitle("Remove Disc.", for: .normal)
        }
        
    }
    
    @IBAction func btnDiscounts(_ sender: Any) {
        
        
        if(self.pos_transaction_header.discount_id != ""){
            self.btnDiscounts.setTitle("Discounts", for: .normal)
            
            self.pos_transaction_header.discount_id = ""
            self.pos_transaction_header.discount_value = "0"
            self.pos_transaction_header.discount_is_percent = "0"
            
            mvTip = 0
            mvDiscount = 0
            mvTax = 0
            mvTaxExempt = 0
            self.ComputeTotal(main_category_id: "0", is_less: true, gross: 0, payment: 0, item_discount: 0, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
            
            
            
        }else{
            self.removeSubviews()
            
            conVw.isHidden = false
            
            let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectDiscount") as! SelectDiscount
            vc.protocolDelegate = self
            vc.is_ticket = true
            
            vc.modalPresentationStyle = .overCurrentContext
            addChildViewController(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
            conVw.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
            
            
            //self.btnDiscounts.setTitle("Remove Disc.", for: .normal)
        }
        

    }

    @IBAction func btnCustomer(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Customer", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CustomerManagement") as! CustomerManagement
        
        vc.protocolDelegate = self
        
        self.present(vc, animated: false, completion: nil)
    }
    
    // Remove all child subviews before addsubviews
     func removeSubviews() {
        for view in self.conVw.subviews{
            view.removeFromSuperview()
        }
    }
    
    @IBAction func btnPayment(_ sender: Any) {
        if(self.ReSaveTransaction()){
            self.removeSubviews()
        
            conVw.isHidden = false
        
            let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TicketPayment") as! TicketPayment
            vc.protocolDelegate = self
            vc.pos_transaction_header = self.pos_transaction_header
            vc.pos_customer = self.pos_customer
        
            vc.modalPresentationStyle = .overCurrentContext
            addChildViewController(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
            conVw.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }
        
    }
    
    @IBAction func btnExit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func InitializeTicket(){

        
       // print("id " + self.pos_customer.id)
        
        
        if(self.pos_customer.id == "-1"){
            
            self.lblMemberId.text = ""
            self.lblPhoneNumber.text = ""
            self.lblEmailAddress.text = ""
            self.lblBirthDate.text = ""
            self.lblDefaultKit.text = ""
            
            self.lblFirstVisit.text = ""
            self.lblLastVisit.text = ""
            self.lblVisitCount.text = ""
            self.lblTotalSpend.text = ""
            self.lblTotalPoints.text = ""
            
        }else{
            
            LoadCustomerToTicket(customer: self.pos_customer)
            
        }
        
        
        
        self.lblTips.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_tip)!, style: .currency)
        self.lblTax.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_tax)!, style: .currency)
        self.lblSubtotal.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_gross)!, style: .currency)
        self.lblDiscounts.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_discount)!, style: .currency)
        self.lblTotalAmount.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_net)!, style: .currency)
        
        self.lblRetail.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_products)!, style: .currency)
        self.lblService.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_services)!, style: .currency)
        self.lblGiftCards.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_gift_cards)!, style: .currency)
        self.lblPackages.text = UIUtils.formatCurrency(value: Double(self.pos_transaction_header.total_packages)!, style: .currency)
        
        if(self.pos_transaction_header.tip_is_percent == "1"){
            //self.pos_transaction_header.tip_is_percent = "1"
            self.lblbTipsLabel.text = "Tips (\(self.pos_transaction_header.total_tip)%):"
        }else{
            //self.pos_transaction_header.tip_is_percent = "0"
            self.lblbTipsLabel.text = "Tips:"
        }
        
        if(self.pos_transaction_header.discount_is_percent == "1"){
            //self.pos_transaction_header.tip_is_percent = "1"
            self.lblDiscountLabel.text = "Discounts (\(self.pos_transaction_header.discount_value)%):"
        }else{
            //self.pos_transaction_header.tip_is_percent = "0"
            self.lblDiscountLabel.text = "Discounts:"
        }
        
        if(self.pos_transaction_header.discount_id != ""){
            self.btnDiscounts.setTitle("Remove Disc.", for: .normal)
        }else{
            self.btnDiscounts.setTitle("Discount", for: .normal)
        }
        
        if(Double(self.pos_transaction_header.total_tip)! > 0){
            self.btnTips.setTitle("Remove Tips", for: .normal)
        }else{
            self.btnTips.setTitle("Tips", for: .normal)
        }
        
        if(Double(self.pos_transaction_header.total_tax_exempt)! > 0){
            mvIsTaxExempt = true
            self.btnTax.setTitle("Tax", for: .normal)
            self.lblTaxLabel.text = "Tax (Exempt):"
        }else{
            mvIsTaxExempt = false
            self.btnTax.setTitle("Remove Tax", for: .normal)
            self.lblTaxLabel.text = "Tax:"
        }
        
        for detail in self.pos_transaction_detail {
            //self.AddItemToListView(main_category_id: main_category_id, item_id: item_id, price: price, item_name: item_name)
            
            self.RecordList.append([
                "id":"\(detail.id)",
                "line_number":"\(detail.line_number)",
                "specialist_id":"\(detail.specialist_id)",
                "commision_id":"\(detail.commision_id)",
                "main_category_id":"\(detail.main_category_id)",
                "item_id":"\(detail.item_id)",
                "quantity":"\(detail.quantity)",
                "price":"\(detail.price)",
                "discount_id":"\(detail.discount_id)",
                "discount_is_percent":"\(detail.discount_is_percent)",
                "discount_value":"\(detail.discount_value)",
                "item_discount":"\(detail.item_discount)",
                "item_gross":"\(detail.item_gross)",
                "item_net":"\(detail.item_net)",
                "item_remarks":"\(detail.item_remarks)",
                "item_name":"\(detail.item_name)",
                "specialist_name":"\(detail.first_name + " " + detail.last_name)"
                ])
            
            
            
            
            tblVw.beginUpdates()
            tblVw.insertRows(at: [IndexPath(row: self.RecordList.count-1, section: 0)], with: .automatic)
            tblVw.endUpdates()
            
            mvTip = 0
            mvDiscount = 0
            mvTax = 0
            mvTaxExempt = 0
            self.ComputeTotal(main_category_id: detail.main_category_id, is_less: false, gross: Double(detail.price)! * Double(detail.quantity)!, payment: 0, item_discount: Double(detail.item_discount)!, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
            
            
        }
        
        
        if(self.pos_transaction_header.id == ""){
            self.lblTicketHeader.text = "NEW TICKET : " + self.employee.first_name + " " + self.employee.last_name
        }else{
            self.lblTicketHeader.text = "TICKET#\(self.pos_transaction_header.doc_code) : " + self.employee.first_name + " " + self.employee.last_name
        }
        
    }
    
    func showDiscountList(){
        self.removeSubviews()
    
        conVw.isHidden = false
    
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SelectDiscount") as! SelectDiscount
        vc.is_ticket = false
        
        vc.modalPresentationStyle = .overCurrentContext
        addChildViewController(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
        conVw.addSubview(vc.view)
    
        vc.didMove(toParentViewController: self)
    }
    
    func showEnterTips(){
        self.removeSubviews()
        
        conVw.isHidden = false
        
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EnterTips") as! EnterTips
        vc.protocolDelegate = self
        
        
        vc.modalPresentationStyle = .overCurrentContext
        addChildViewController(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
        conVw.addSubview(vc.view)
        
        vc.didMove(toParentViewController: self)
    }
    
    func showTicketItems()
    {
        self.removeSubviews()
        
        conVw.isHidden = false
        
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TicketItems") as! TicketItems
        vc.protocolDelegate = self
        
        vc.modalPresentationStyle = .overCurrentContext
        addChildViewController(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: conVw.frame.size.width, height: conVw.frame.size.height)
        conVw.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    func LoadCustomerToTicket(customer:PosCustomers){
        
        let nsf = NumberFormatter()
        nsf.minimumIntegerDigits = 8
        
        
        self.pos_customer = customer
        
        self.pos_transaction_header.customer_id = self.pos_customer.id
        self.lblMemberId.text = nsf.string(from: NSNumber(value: Int(self.pos_customer.id)!))
        self.lblPhoneNumber.text = self.pos_customer.mobile_number
        self.lblEmailAddress.text = self.pos_customer.email_address
        self.lblBirthDate.text = self.pos_customer.birthdate
        self.lblDefaultKit.text = self.pos_customer.default_kit_id
        
        self.lblFirstVisit.text = self.pos_customer.first_visit_date
        self.lblLastVisit.text = self.pos_customer.last_visit_date
        self.lblVisitCount.text = self.pos_customer.visit_count
        self.lblTotalSpend.text = self.pos_customer.total_spent
        self.lblTotalPoints.text = self.pos_customer.loyalty_points
        
        
        self.txtCustomer.text = self.pos_customer.first_name + " " + self.pos_customer.last_name
    }
    
    
}

extension Ticket:TicketItemsViewControllerDelegate {
    func didOpenTicketItems(viewController vc: TicketItems, main_category_id: String, item_id: String, price: String, item_name: String)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        //if success
        //{
            //vc.dismiss(animated: true, completion: nil)
            
            self.AddItemToListView(main_category_id: main_category_id, item_id: item_id, price: price, item_name: item_name)
            
            
        
            
        //}
        
    }
}

extension Ticket:SelectTicketItemViewControllerDelegate {
    func didOpenSelectTicketItem(viewController vc: SelectTicketItem, success: Bool, method: String, quantity:String, price:String, discount_id: String, is_percent : String, item_discount:String, item_remarks:String, employee_id:String, employee_name:String)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)

            if(method == "delete"){
                self.DeleteItemToListView()
                //vc.dismiss(animated: true, completion: nil)
            }else if(method == "edit"){
                self.EditItemToListView(quantity: quantity, price: price, discount_id:discount_id, is_percent: is_percent, item_discount: item_discount, item_remarks: item_remarks, employee_id: employee_id, employee_name: employee_name)
                //vc.dismiss(animated: true, completion: nil)
            }else if(method == "add_discount"){
                self.showDiscountList()
            }
            
            
            
            
            
            
        }
        
    }
}

extension Ticket:SelectDiscountViewControllerDelegate {
    func didOpenSelectDiscount(viewController vc: SelectDiscount, success: Bool, discount_id: String, value : String, is_percent : String)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        self.showTicketItems()
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)
            
            //self.showDiscountList()
            
            if(is_percent == "1"){
                self.pos_transaction_header.discount_is_percent = "1"
                self.lblDiscountLabel.text = "Discounts (\(value)%):"
            }else{
                self.pos_transaction_header.discount_is_percent = "0"
                self.lblDiscountLabel.text = "Discounts:"
            }
            
            self.pos_transaction_header.discount_value = value
            self.pos_transaction_header.discount_id = discount_id
            
            self.btnDiscounts.setTitle("Remove Disc.", for: .normal)
            
            
            //self.lblTips.text = amount
            mvTip = 0
            mvDiscount = 0
            mvTax = 0
            mvTaxExempt = 0
            self.ComputeTotal(main_category_id: "0", is_less: true, gross: 0, payment: 0, item_discount: 0, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
            
        }
        
    }
    
    
    
}

extension Ticket:ModalAddTipsViewControllerDelegate {
    func didOpenModalAddTips(viewController vc: ModalAddTips, button_str: String)
    {
        vc.dismiss(animated: true, completion: nil)
        
        if (button_str == "tips"){
            self.showEnterTips()
        }else if(button_str == "tax"){
            //remove or add tax
        }
        
    }
}

extension Ticket:ModalQuickCashViewControllerDelegate {
    func didOpenModalQuickCash(viewController vc: ModalQuickCash, success: Bool)
    {
        vc.dismiss(animated: true, completion: nil)
        
        if(success){
            self.protocolDelegate?.didOpenTicket(viewController: self, success: true)
        }
        

    }
}






extension Ticket:EnterTipsViewControllerDelegate {
    func didOpenEnterTips(viewController vc: EnterTips, success: Bool, amount: String, is_percent: Bool)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        self.showTicketItems()
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)
            
            //self.showDiscountList()
            if(is_percent){
                self.pos_transaction_header.tip_is_percent = "1"
                self.lblbTipsLabel.text = "Tips (\(amount)%):"
            }else{
                self.pos_transaction_header.tip_is_percent = "0"
                self.lblbTipsLabel.text = "Tips:"
            }
            
            self.pos_transaction_header.total_tip = amount
            
            self.btnTips.setTitle("Remove Tips", for: .normal)
            
            
            //self.lblTips.text = amount
            mvTip = 0
            mvDiscount = 0
            mvTax = 0
            mvTaxExempt = 0
            self.ComputeTotal(main_category_id: "0", is_less: true, gross: 0, payment: 0, item_discount: 0, discount: 0, tip: 0, cost: 0, cash: 0, cheque: 0, credit_card: 0, debit_card: 0, loyalty: 0, cash_tendered: 0, others_tendered: 0)
            
            
        }
        
    }
}

extension Ticket:TicketPaymentViewControllerDelegate {
    func didOpenTicketPayment(viewController vc: TicketPayment, success: Bool)
    {
        vc.dismiss(animated: true, completion: nil)
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)

            self.protocolDelegate?.didOpenTicket(viewController: self, success: true)
            
        }
        
    }
}

extension Ticket:CustomerManagementViewControllerDelegate {
    func didOpenCustomerManagement(viewController vc: CustomerManagement, success: Bool, customer:PosCustomers)
    {
        vc.dismiss(animated: true, completion: nil)
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)
            
            self.LoadCustomerToTicket(customer: customer)
            
        }
        
    }
}

extension Ticket:SelectVoidReasonViewControllerDelegate {
    func didOpenSelectVoidReason(viewController vc: SelectVoidReason, success: Bool, void_reason_id: String)
    {
        //vc.dismiss(animated: true, completion: nil)
        
        //self.showTicketItems()
        
        if success
        {
            //vc.dismiss(animated: true, completion: nil)
            
            if(self.pos_transaction_header.status != "S"){
                MyAlert.displayAlertWithDismiss("Ticket", msg: "Nothing to void.", controller: self)
            }else{
                let response:Dictionary<String, Any> = self.VoidTransaction(id: self.pos_transaction_header.id, void_reason_id: void_reason_id)
                
                let alert = UIAlertController(title: "Ticket", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    //self.dismiss(animated: false, completion: nil)
                    if(response["success"]as? Bool)!{
                        self.protocolDelegate?.didOpenTicket(viewController: self, success: true)
                    }
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            
            //self.showDiscountList()
            
            
            
        }
        
    }
    
    
    
}


