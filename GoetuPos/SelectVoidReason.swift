//
//  SelectVoidReason.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/16/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit

protocol SelectVoidReasonViewControllerDelegate {
    func didOpenSelectVoidReason(viewController vc: SelectVoidReason, success: Bool, void_reason_id: String)
}

class VoidReasonCell: UICollectionViewCell {
    
    @IBOutlet weak var lblVoidReasonName: UILabel!
    
}

class SelectVoidReason: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var colVw: UICollectionView!
    
    var protocolDelegate : SelectVoidReasonViewControllerDelegate?
    
    var RecordList: [[String:String]] = [[String:String]]()
    
    var progressView = CustomProgressView()
    
    let date = Date()
    let format = DateFormatter()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getVoidReasons()

        
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return RecordList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VoidReasonCell", for: indexPath) as! VoidReasonCell
        
        let row = indexPath.row
        let obj = RecordList[row] as NSDictionary
        
        //let id = obj["id"] as! String
        let name = obj["name"] as! String
        //
        
        cell.lblVoidReasonName.text = name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.protocolDelegate?.didOpenSelectVoidReason(viewController: self, success: true, void_reason_id: self.RecordList[indexPath.row]["id"]!)
        
    }
    
    func getVoidReasons(){
        
        if (self.RecordList.count > 0) {
            self.RecordList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from pos_void_reasons where status = 'A'"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            let id = rs.string(forColumn: "id") ?? ""
            let name = rs.string(forColumn: "name") ?? ""
            
            
            let obj = [
                "id":"\(id)",
                "name":"\(name)",
            ]
            self.RecordList.append(obj)
            
            
            
            
        }
        
        
        self.colVw.reloadData()
        //    self.colVwSpecialist.reloadData()
        //    self.colVwInService.reloadData()
        db.closeDB()
    }    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        
        
    }
    



}
