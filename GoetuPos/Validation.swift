//
//  Validation.swift
//  Go3Reservation
//
//  Created by Luigi Guevarra on 6/30/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import Foundation
import UIKit

class Validation {
    
    func checkFileNoLength(_ check: String!, length: Int!, name: String!, controller: UIViewController) -> Bool {
        if (check.characters.count != length) {
            let message = "Please enter a \(length!) digits " + name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] +  "."
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func checkMatches(_ first: String!, second: String!, name_first: String!, name_second: String!, controller: UIViewController) -> Bool {
        if (first != second) {
            let message = name_second.substring(to: name_second.index(name_second.startIndex, offsetBy: 1)).capitalized + name_second[name_second.index(name_second.startIndex, offsetBy: 1)...name_second.index(name_second.startIndex, offsetBy: name_second.characters.count - 1)] +  " does not match with " + name_first
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func checkExactLength(_ check: String!, length: Int!, name: String!, controller: UIViewController) -> Bool {
        if (check.characters.count != length) {
            let message = "Please enter a " + name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] +  " with \(length!) characters long."
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func checkMinLength(_ check: String!, length: Int!, name: String!, controller: UIViewController) -> Bool {
        if (check.characters.count < length) {
            let message = "Please enter a " + name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] +  " with at least \(length!) characters long."
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func checkIfEmpty(_ check: String!, name: String!, controller: UIViewController) -> Bool {
        if (check.isEmpty) {
            let message = name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] +  " should not be empty."
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func checkIfNil(_ check: String!, name: String!, controller: UIViewController) -> Bool {
        if (check == nil) {
            let message = name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] +  " should not be empty."
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    
    
    func checkShouldNotContain(_ check: String!, character_string: String!, name: String!, controller: UIViewController) -> Bool {
        if (check.range(of: character_string) != nil) {
            var message = ""
            if (character_string.range(of: " ") != nil) {
                message = name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] + " should not contain space."
            } else {
                message = name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] + " should not contain \(character_string)"
            }
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func checkSpecialCharacters(_ check: String!, name: String!, controller: UIViewController) -> Bool {
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        if regex.firstMatch(in: check!, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, check!.characters.count)) != nil {
            let message = name.substring(to: name.index(name.startIndex, offsetBy: 1)).capitalized + name[name.index(name.startIndex, offsetBy: 1)...name.index(name.startIndex, offsetBy: name.characters.count - 1)] + " should not contain special characters or space."
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func checkEmail(_ check: String!, name: String!, controller: UIViewController) -> Bool {
        if !validateEmail(check) {
            let message = "Invalid format for your \(name)."
            displayAlert(message, cnt: controller)
            return false
        }
        return true
    }
    
    func validateEmail(_ candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    func displayAlert(_ message: String, cnt: UIViewController) {
        MyAlert.displayAlert("Check Input", msg: message, controller: cnt)
    }
    
}

