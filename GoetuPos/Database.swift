import Foundation
import UIKit
import CoreData

import SwiftyJSON
import Alamofire

class Database {
    
    var posDB: FMDatabase
    
    
    //    func openDatabase() -> FMDatabase
    //    func openDatabase()
    //    {
    
    //        posDB.open()
    //        return posDB!
    //    }
    
    
    
    func setDBSetting(_ setting_name: String, setting_value: String) -> String
    {
        var cmd = "SELECT * FROM system_setting WHERE setting_name='\(setting_name)'"
        let rs:FMResultSet? = posDB.executeQuery(cmd, withArgumentsIn: nil)
        if  (rs?.next() == false)
        {
            cmd = "INSERT INTO system_setting (setting_name, setting_value) values('\(setting_name)', '\(setting_value)')"
            _ = posDB.executeStatements(cmd)
        }else{
            if setting_value != ""
            {
                cmd = "UPDATE system_setting SET setting_value='\(setting_value)' WHERE setting_name = '\(setting_name)'"
                _ = posDB.executeStatements(cmd)
            }
        }
        return setting_value
    }
    
    func getDBSetting(_ setting_name: String) -> String
    {
        var setting_value = ""
        let cmd = "SELECT * FROM system_setting WHERE setting_name='\(setting_name)'"
        let rs:FMResultSet? = posDB.executeQuery(cmd, withArgumentsIn: nil)
        if  (rs?.next() == true)
        {
            setting_value = rs?.string(forColumn: "setting_value") ?? ""
        }
        return setting_value
    }
    
    
    func create_additional_tables()
    {
        
        //LC-07/12/2017
        posDB.executeStatements("ALTER TABLE employee ADD COLUMN access_code TEXT")
        var cmd = ""
        cmd = " CREATE TABLE IF NOT EXISTS clock_log (id INTEGER PRIMARY KEY AUTOINCREMENT, employee_id INTEGER,  create_date REAL, in_log DECIMAL, out_log DECIMAL, status TEXT, counter_position INTEGER, total_service INTEGER, total_amount DECIMAL, total_commission_amount DECIMAL, total_tip_amount DECIMAL)"
        posDB.executeStatements(cmd)
        
        //        if posDB.hadError() {
        //            print("DBERROR:")
        //            print(posDB.lastError())
        //            print("\n")
        //        }
        
        
        //        ALTER TABLE user ADD COLUMN user_type_id INTEGER
        posDB.executeStatements("ALTER TABLE employee_schedule ADD COLUMN is_open TEXT")
        posDB.executeStatements("ALTER TABLE transaction_header ADD COLUMN or_no TEXT")
        posDB.executeStatements("ALTER TABLE product ADD COLUMN commission_id INTEGER")
        posDB.executeStatements("ALTER TABLE user ADD COLUMN user_type_id INTEGER")
        
        //Create TransactionItem
        posDB.executeStatements("CREATE TABLE IF NOT EXISTS  `transaction_item` ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `header_id` INTEGER, `discount_value` TEXT, `discount_id` INTEGER, `employee_id` INTEGER, `commission_id` INTEGER, `commission_value` TEXT, `tax_amount` NUMERIC, `service_id` INTEGER, `price` NUMERIC, `quantity` INTEGER )")
        
        //Create TransactionPayment
        posDB.executeStatements("CREATE TABLE IF NOT EXISTS `transaction_payment` ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `header_id` INTEGER, `amount` NUMERIC, `payment_name` TEXT, `card_name` TEXT, `card_last_name` TEXT, `card_first_name` TEXT, `card_expiry` TEXT, `card_number` TEXT, `amount_received` NUMERIC )")
        
        //Create TransactionHeader
        posDB.executeStatements("CREATE TABLE IF NOT EXISTS `transaction_header` ( `header_id` INTEGER PRIMARY KEY AUTOINCREMENT, `customer_id` INTEGER, `total_discount_amount` NUMERIC, `total_tax_amount` NUMERIC, `total_amount` NUMERIC, `total_net_amount` BLOB, `status` TEXT, `create_date` REAL, `create_by` TEXT, `update_date` REAL, `update_by` TEXT )")
        
        
        var create_cmd = "CREATE TABLE IF NOT EXISTS merchant_info (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `salon_name` TEXT, `salon_description` TEXT, telephone_no TEXT, mobile_no TEXT, fax TEXT, email_address TEXT, contact_person TEXT, salon_logo TEXT, address TEXT, country TEXT, city TEXT, state TEXT, zip TEXT)"
        posDB.executeStatements(create_cmd)
        
        create_cmd = "CREATE TABLE IF NOT EXISTS system_setting (id INTEGER PRIMARY KEY AUTOINCREMENT, setting_name TEXT, setting_value TEXT)"
        posDB.executeStatements(create_cmd)
        
        create_cmd = "CREATE TABLE IF NOT EXISTS user_type (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT)"
        posDB.executeStatements(create_cmd)
        
        create_cmd = "CREATE TABLE IF NOT EXISTS user_module (id INTEGER PRIMARY KEY AUTOINCREMENT, module_code TEXT, module_name TEXT, description TEXT)"
        posDB.executeStatements(create_cmd)
        
        create_cmd = "CREATE TABLE IF NOT EXISTS user_type_module_access (id INTEGER PRIMARY KEY AUTOINCREMENT, user_type_id INTEGER, user_module_id INTEGER)"
        posDB.executeStatements(create_cmd)
        
        create_cmd = "CREATE TABLE IF NOT EXISTS supported_printer (id INTEGER PRIMARY KEY AUTOINCREMENT, brand_name TEXT, printer_name TEXT, mac_address TEXT, port_name TEXT, port_type TEXT, timeout INTEGER, has_cash_drawer TEXT, emulation TEXT)"
        posDB.executeStatements(create_cmd)
    }
    
    func add_attendance(business_date : String, specialist_id : String, notes : String){
        let cmd = "INSERT INTO attendance (business_date, specialist_id, notes) VALUES('\(business_date)', \(specialist_id), '\(notes)')"
        posDB.executeStatements(cmd)
    }
    
    func add_specialist_on_queue(specialist_id : String, order_number : String, total_count : String, total_amount : String, status : String){
        //let cmd = "INSERT INTO specialist_on_queue (specialist_id, order_number, total_count, total_amount, status) VALUES(\(specialist_id), \(order_number), \(total_count), \(total_amount), '\(status)')"
        
        var cmd = ""
        
        if(order_number == "-1"){
             cmd = "INSERT INTO specialist_on_queue SELECT \(specialist_id), (select count(*) from specialist_on_queue)+1, \(total_count), \(total_amount), '\(status)', -1"
        }else{
             cmd = "INSERT INTO specialist_on_queue (specialist_id, order_number, total_count, total_amount, status, transaction_id) VALUES(\(specialist_id), \(order_number), \(total_count), \(total_amount), '\(status)', -1)"
        }
        

        
        
        posDB.executeStatements(cmd)
    }
    
    func delete_specialist_on_queue(specialist_id : String){
        let cmd = "delete from specialist_on_queue where specialist_id = \(specialist_id)"
        posDB.executeStatements(cmd)
    }
    
    func update_status_specialist_on_queue(specialist_id : String, status : String){
        let cmd = "update specialist_on_queue set status = '\(status)' where specialist_id = \(specialist_id)"
        posDB.executeStatements(cmd)
    }
    
    func update_order_number_specialist_on_queue(specialist_id : String, order_number : String){
        let cmd = "update specialist_on_queue set order_number = '\(order_number)' where specialist_id = \(specialist_id)"
        posDB.executeStatements(cmd)
    }
    
    func renumber_specialist_on_queue() {
        
        //var specialistList: [[String:String]] = [[String:String]]()
        var cmd = ""
        var i = 1
        
        cmd = "SELECT * FROM specialist_on_queue order by order_number asc"
        
        let rs:FMResultSet? = posDB.executeQuery(cmd, withArgumentsIn: nil)
        
//        if  (rs?.next() == true){
//            cmd = "delete from specialist_on_queue"
//            posDB.executeStatements(cmd)
//        }

        while (rs?.next())! {
            
            let specialist_id = rs?.string(forColumn: "specialist_id") ?? ""
            //let order_number = rs?.string(forColumn: "order_number") ?? ""
            let total_count = rs?.string(forColumn: "total_count") ?? ""
            let total_amount = rs?.string(forColumn: "total_amount") ?? ""
            let status = rs?.string(forColumn: "status") ?? ""

//            let obj = [
//                "specialist_id":"\(specialist_id)",
//                "order_number":"\(order_number)",
//                "total_count":"\(total_count)",
//                "total_amount":"\(total_amount)",
//                "status":"\(status)",
//            ]
//            specialistList.append(obj)
            
            
            self.update_order_number_specialist_on_queue(specialist_id: specialist_id, order_number: String(i))
            
            i = i + 1
            
        }
        
        //return specialistList

    }
    
    func insert_system_module_default_printer()
    {
        
        //category
        var insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('category', 'CATEGORIES', 'Module for managing categories')"
        posDB.executeStatements(insert)
        
        //service
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('service', 'SERVICES', 'Module for managing services')"
        posDB.executeStatements(insert)
        
        //waitlist
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('waitlist', 'WAITING LIST', 'Module for waiting list that is connected on reservation')"
        posDB.executeStatements(insert)
        
        //reservation
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('reservation', 'RESERVATIONS', 'Module for reservation that is connected on reservation system')"
        posDB.executeStatements(insert)
        
        //employee
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('employee', 'EMPLOYEE', 'Module for managing employee')"
        posDB.executeStatements(insert)
        
        //discount
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('discount', 'DISCOUNT', 'Module for managing discount')"
        posDB.executeStatements(insert)
        
        //transaction
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('transaction', 'TRANSACTIONS', 'Module for sales transaction')"
        posDB.executeStatements(insert)
        
        //commission
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('commission', 'COMMISSION', 'Module for managing commissions')"
        posDB.executeStatements(insert)
        
        
        //reports
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('reports', 'REPORTS', 'Module for reports')"
        posDB.executeStatements(insert)
        
        
        //customer
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('customer', 'CUSTOMER', 'Module for customer')"
        posDB.executeStatements(insert)
        
        //admin
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('admin', 'ADMIN', 'Module for admin')"
        posDB.executeStatements(insert)
        
        
        //transaction history
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('transaction_history', 'TRANSACTION HISTORY', 'Module for transaction history with capabilities of voiding')"
        posDB.executeStatements(insert)
        
        //admin
        //access rights
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('access_right', 'ACCESS RIGHTS', 'Module for defining group access rights')"
        posDB.executeStatements(insert)
        
        //receipt settings
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('receipt_settings', 'RECEIPT SETTINGS', 'Module for configuring receipt')"
        posDB.executeStatements(insert)
        
        //merchant profile
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('merchant_profile', 'MERCHANT INFORMATION', 'Module for editing merchant information')"
        posDB.executeStatements(insert)
        
        
        //other settings
        insert = "INSERT INTO user_module(module_code, module_name, description) VALUES('other_settings', 'OTHER SETTINGS', 'Module for defining other settings of the POS like enable discount, commissions and etc.')"
        posDB.executeStatements(insert)
        
        //supported_printers
        insert = "INSERT INTO supported_printer (brand_name, printer_name, mac_address, port_name, port_type, timeout, has_cash_drawer, emulation) VALUES ('Star POS Printer', 'SM-T300i', '', 'BT:PRNT Star', 'mini', 10000, 'true','escPosMobile')"
        posDB.executeStatements(insert)
    }
    
    
    
    
    
    func getLastInsertID() -> String
    {
        let id = posDB.lastInsertRowId()
        let strId = "\(id)"
        return strId
    }
    
    func getResults(_ cmd: String) -> FMResultSet
    {
        let rs:FMResultSet? = posDB.executeQuery(cmd, withArgumentsIn: nil)
        return rs!
        
    }
    
    
    func executeStatement(_ cmd: String) -> Bool
    {
        let res = posDB.executeStatements(cmd)
        return res
    }
    
    func executeNonQuery(_ cmd: String)
    {
        let res = posDB.executeStatements(cmd)
    }
    
    
    init()
    {
        
        //        let doumentDirectoryPath = NSSearchPathForDirectoriesInDomains(.developerDirectory, .systemDomainMask, true)[0] as! NSString
        //        let destinationPath1 = doumentDirectoryPath.appendingPathComponent("pos.db")[0]
        let filemgr = FileManager.default
        
        //        let
        //        let documentPath = filemgr.urls(for: .documentDirectory, in: .allDomainsMask)
        //        let destiPath = documentPath[0].appendingPathComponent("pos1.db").path
        //        print(documentPath)
        
        //        let filemgr = FileManager.default
        //
        //        if filemgr.fileExists(atPath: "/Applications") {
        //            print("File exists")
        //        } else {
        //            print("File not found")
        //        }
        //
        //        if filemgr.isWritableFile(atPath: "/Applications") {
        //            print("File is writable")
        //        } else {
        //            print("File is read-only")
        //        }
        
        
        let applicationSupportFolderURL = try! FileManager.default.url(for: .applicationSupportDirectory,
                                                                       in: .localDomainMask,
                                                                       appropriateFor: nil,
                                                                       create: false)
        print(applicationSupportFolderURL)
        
        
        let dirPath = filemgr.urls(for: .documentDirectory, in: .userDomainMask)
        
        let filePath = dirPath[0].path
        
        let path : NSString = filePath as NSString
        
        let thePath = path.expandingTildeInPath
        
        print(thePath)
        
        
        
        let source = Bundle.main.url(forResource: "pos", withExtension: ".db")
        
        //print(source)
        
//                let bundle1 = Bundle.main.bundlePath
//        
//                print(bundle1)
        
        
        
        
        let dbSource1 = (source?.path)!
        
        
        do{
            try filemgr.copyItem(atPath: dbSource1, toPath: "/Applications/luigi.db")
        }catch let error{
            print("Error: \(error.localizedDescription)")
        }
        
        if filemgr.fileExists(atPath: "/Applications/luigi.db") {
            print("File exists")
        } else {
            print("File not found")
        }
        
        
        var databasePath: String = ""
        
        databasePath = dirPath[0].appendingPathComponent("pos.db").path
        
        
        
        
        var dbSource: String = ""
        dbSource = (source?.path)!
        
        //        print("\nPATH:")
        //        print(dbSource)
        //        print("\n")
        //
        print("\nPATH:")
        print(databasePath)
        print("\n")
        
        MyDefaults.saveString("db_path", value: databasePath)
        
        if(!filemgr.fileExists(atPath: databasePath as String))
        {
            do{
                try filemgr.copyItem(atPath: dbSource, toPath: databasePath)
            }catch let error{
                print("Error: \(error.localizedDescription)")
            }
            posDB = FMDatabase(path: databasePath as String)
        }else{
            
            posDB = FMDatabase(path: databasePath as String)
            
        }
        posDB.open()
        
        
    }
    
    func closeDB()
    {
        posDB.close()
    }
    
    
    
}
