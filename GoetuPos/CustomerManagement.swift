//
//  CustomerManagement.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/20/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit


protocol CustomerManagementViewControllerDelegate {
    func didOpenCustomerManagement(viewController vc: CustomerManagement, success: Bool, customer:PosCustomers)
}

class CustomerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblBirthDate: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!


}

class CustomerManagement: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtBirthDate: UITextField!
    @IBOutlet weak var txtNotes: UITextView!
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    
    @IBOutlet weak var vwHeader: UIView!
    
    
    
    var protocolDelegate : CustomerManagementViewControllerDelegate?
    
    var pos_customers = PosCustomers()
    
    var validate = Validation()
    var progressView = CustomProgressView()
    var RecordList: [[String:String]] = [[String:String]]()
    var SearchList: [[String:String]] = [[String:String]]()
    var selectedIndex = 0
    
    let date = Date()
    let format = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblVw.tableHeaderView = self.vwHeader
        
        
        self.getCustomers()
        
        //ok
        let x_pos = (btnOk.frame.size.width / 2) - 16
        
        let leftImageView1 = UIImageView()
        leftImageView1.image = UIImage(named: "ok")
        leftImageView1.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnOk.addSubview(leftImageView1)
        
        let leftImageView2 = UIImageView()
        leftImageView2.image = UIImage(named: "cancel")
        leftImageView2.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnExit.addSubview(leftImageView2)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func btnOk(_ sender: Any) {
        
        
        self.pos_customers.id = self.SearchList[selectedIndex]["id"]!
        self.pos_customers.first_name = self.SearchList[selectedIndex]["first_name"]!
        self.pos_customers.last_name = self.SearchList[selectedIndex]["last_name"]!
        self.pos_customers.status = self.SearchList[selectedIndex]["status"]!
        self.pos_customers.notes = self.SearchList[selectedIndex]["notes"]!
        self.pos_customers.gender = self.SearchList[selectedIndex]["gender"]!
        self.pos_customers.civil_status = self.SearchList[selectedIndex]["civil_status"]!
        self.pos_customers.address = self.SearchList[selectedIndex]["address"]!
        self.pos_customers.state_id = self.SearchList[selectedIndex]["state_id"]!
        self.pos_customers.city_id = self.SearchList[selectedIndex]["city_id"]!
        self.pos_customers.zip_code = self.SearchList[selectedIndex]["zip_code"]!
        self.pos_customers.member_id = self.SearchList[selectedIndex]["member_id"]!
        self.pos_customers.mobile_number = self.SearchList[selectedIndex]["mobile_number"]!
        self.pos_customers.email_address = self.SearchList[selectedIndex]["email_address"]!
        self.pos_customers.birthdate = self.SearchList[selectedIndex]["birthdate"]!
        self.pos_customers.default_kit_id = self.SearchList[selectedIndex]["default_kit_id"]!
        self.pos_customers.first_visit_date = self.SearchList[selectedIndex]["first_visit_date"]!
        self.pos_customers.last_visit_date = self.SearchList[selectedIndex]["last_visit_date"]!
        self.pos_customers.visit_count = self.SearchList[selectedIndex]["visit_count"]!
        self.pos_customers.total_spent = self.SearchList[selectedIndex]["total_spent"]!
        self.pos_customers.loyalty_points = self.SearchList[selectedIndex]["loyalty_points"]!
        self.pos_customers.create_date = self.SearchList[selectedIndex]["create_date"]!
        self.pos_customers.create_by = self.SearchList[selectedIndex]["create_by"]!
        self.pos_customers.update_date = self.SearchList[selectedIndex]["update_date"]!
        self.pos_customers.update_by = self.SearchList[selectedIndex]["update_by"]!
        
        self.protocolDelegate?.didOpenCustomerManagement(viewController: self, success: true, customer: self.pos_customers)
        
    }

    @IBAction func btnExit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SearchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblVw.dequeueReusableCell(withIdentifier: "CustomerTableViewCell", for: indexPath) as! CustomerTableViewCell
        
        //let total_sms:Int = Int(RecordList[indexPath.row]["quantity"]!)! * Int(RecordList[indexPath.row]["package_quantity"]!)!
        
        cell.lblFirstName.text = SearchList[indexPath.row]["first_name"]!
        cell.lblLastName.text = SearchList[indexPath.row]["last_name"]!
        cell.lblMobileNumber.text = SearchList[indexPath.row]["mobile_number"]!
        cell.lblBirthDate.text = SearchList[indexPath.row]["birthdate"]!
        cell.lblEmailAddress.text = SearchList[indexPath.row]["email_address"]!
        //UIUtils.formatCurrency(value: self.mvTip, style: .currency)
        
        cell.tag = indexPath.row
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return vwHeader
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        

        
        selectedIndex = indexPath.row
        

        
    }
    
    func SearchByText(){
        
        //if (self.txtFirstName.text! == "" && self.txtLastName.text! == "" && self.txtBirthDate.text! == "" && self.txtEmail.text! == "") {
        SearchList.removeAll()
        
            let search_first_name:String = self.txtFirstName.text!.lowercased()
            let search_last_name:String = self.txtLastName.text!.lowercased()
            let search_mobile_number:String = self.txtPhoneNumber.text!.lowercased()
            let search_email:String = self.txtEmail.text!.lowercased()
            
        if(search_first_name == "" && search_last_name == "" && search_mobile_number == "" && search_email == ""){
        
            SearchList = RecordList

        } else {
    
    
    
            for strCountry in RecordList {
    
                let first_name = strCountry["first_name"]?.lowercased()
                let last_name = strCountry["last_name"]?.lowercased()
                let mobile_number = strCountry["mobile_number"]?.lowercased()
                let email_address = strCountry["email_address"]?.lowercased()
                
                    SearchList.append(strCountry)
    
                    if( (!first_name!.contains(search_first_name) && (self.txtFirstName.text != "")) ||  (!last_name!.contains(search_last_name) && (self.txtLastName.text != "")) || (!mobile_number!.contains(search_mobile_number) && (self.txtPhoneNumber.text != "")) || (!email_address!.contains(search_email) && (self.txtEmail.text != ""))) {
                        SearchList.removeLast()
                    }
                
//                    if( !last_name!.contains(search_last_name) && self.txtLastName.text != "") {
//    
//                        SearchList.removeLast()
//                    }
//    
//                    if((!mobile_number!.contains(search_mobile_number)) && self.txtPhoneNumber.text != "") {
//    
//                        SearchList.removeLast()
//                    }
//    
//                    if((!email_address!.contains(search_email)) && self.txtEmail.text != "") {
//    
//                        SearchList.removeLast()
//                    }
    
//                } else {
//                    print("no match")
//                }
    
            }
    
    
        }
        
        self.tblVw.reloadData()
        
    }

    @IBAction func txtFirstName(_ sender: UITextField) {
        
        SearchByText()
        
        
    }
    
    @IBAction func txtLastName(_ sender: UITextField) {
        
        
        
        SearchByText()
        
        
        
        
    }
    
    @IBAction func txtMobileNumber(_ sender: UITextField) {
        SearchByText()
    }
    
    @IBAction func txtEmailAddress(_ sender: UITextField) {
        SearchByText()
    }
    
    
    func getCustomers(){
        
        
        if (self.RecordList.count > 0) {
            self.RecordList.removeAll()
        }
        
        var cmd = ""
        
        cmd = "select * from pos_customers where status = 'A'"
        
        let db = Database()
        
        let rs = db.getResults(cmd)
        
        while (rs.next()) {
            
            
            
            let id = rs.string(forColumn: "id") ?? ""
            let first_name = rs.string(forColumn: "first_name") ?? ""
            let last_name = rs.string(forColumn: "last_name") ?? ""
            let status = rs.string(forColumn: "status") ?? ""
            let notes = rs.string(forColumn: "notes") ?? ""
            let gender = rs.string(forColumn: "gender") ?? ""
            let civil_status = rs.string(forColumn: "civil_status") ?? ""
            let address = rs.string(forColumn: "address") ?? ""
            let state_id = rs.string(forColumn: "state_id") ?? ""
            let city_id = rs.string(forColumn: "city_id") ?? ""
            let zip_code = rs.string(forColumn: "zip_code") ?? ""
            let member_id = rs.string(forColumn: "member_id") ?? ""
            let mobile_number = rs.string(forColumn: "mobile_number") ?? ""
            let email_address = rs.string(forColumn: "email_address") ?? ""
            let birthdate = rs.string(forColumn: "birthdate") ?? ""
            let default_kit_id = rs.string(forColumn: "default_kit_id") ?? ""
            let first_visit_date = rs.string(forColumn: "first_visit_date") ?? ""
            let last_visit_date = rs.string(forColumn: "last_visit_date") ?? ""
            let visit_count = rs.string(forColumn: "visit_count") ?? ""
            let total_spent = rs.string(forColumn: "total_spent") ?? ""
            let loyalty_points = rs.string(forColumn: "loyalty_points") ?? ""
            let create_date = rs.string(forColumn: "create_date") ?? ""
            let create_by = rs.string(forColumn: "create_by") ?? ""
            let update_date = rs.string(forColumn: "update_date") ?? ""
            let update_by = rs.string(forColumn: "update_by") ?? ""
            
            
            let obj = [
                "id":"\(id)",
                "first_name":"\(first_name)",
                "last_name":"\(last_name)",
                "status":"\(status)",
                "notes":"\(notes)",
                "gender":"\(gender)",
                "civil_status":"\(civil_status)",
                "address":"\(address)",
                "state_id":"\(state_id)",
                "city_id":"\(city_id)",
                "zip_code":"\(zip_code)",
                "member_id":"\(member_id)",
                "mobile_number":"\(mobile_number)",
                "email_address":"\(email_address)",
                "birthdate":"\(birthdate)",
                "default_kit_id":"\(default_kit_id)",
                "first_visit_date":"\(first_visit_date)",
                "last_visit_date":"\(last_visit_date)",
                "visit_count":"\(visit_count)",
                "total_spent":"\(total_spent)",
                "loyalty_points":"\(loyalty_points)",
                "create_date":"\(create_date)",
                "create_by":"\(create_by)",
                "update_date":"\(update_date)",
                "update_by":"\(update_by)",
            ]
            self.RecordList.append(obj)
            
            
            
            
        }
        
        self.SearchList = self.RecordList
        
        self.tblVw.reloadData()
        //    self.colVwSpecialist.reloadData()
        //    self.colVwInService.reloadData()
        db.closeDB()
        
    }
    




}
