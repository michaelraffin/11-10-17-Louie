//
//  UserData.swift
//  Go3Reservation
//
//  Created by Luigi Guevarra on 5/1/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import Foundation

class UserData : NSObject {
    
    override init() {
        super.init()
    }
    
    class func getPermission() -> String! {
        let defaults = UserDefaults.standard
        let str = defaults.string(forKey: "user_permissions")
        return str
    }
    
    class func getSessionID() -> String! {
        let defaults = UserDefaults.standard
        let str = defaults.string(forKey: "user_session_id")
        return str
    }
    
    class func getFirstName() -> String! {
        let defaults = UserDefaults.standard
        let str = defaults.string(forKey: "user_fname")
        return str
    }
    
    class func getLastName() -> String! {
        let defaults = UserDefaults.standard
        let str = defaults.string(forKey: "user_lname")
        return str
    }
    
}
