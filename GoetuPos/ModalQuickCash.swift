//
//  ModalQuickCash.swift
//  GoetuPos
//
//  Created by Luigi Guevarra on 8/17/17.
//  Copyright © 2017 Luigi Guevarra. All rights reserved.
//

import UIKit

protocol ModalQuickCashViewControllerDelegate {
    func didOpenModalQuickCash(viewController vc: ModalQuickCash, success: Bool)
}

class ModalQuickCash: UIViewController {

    
    @IBOutlet weak var btnPrintReceipt: UIButton!
    @IBOutlet weak var btnNoReceipt: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var validate = Validation()
    var progressView = CustomProgressView()
    //var RecordList: [[String:String]] = [[String:String]]()
    var selectedIndex = 0
    var isEdit:Bool = false
    
    let date = Date()
    let format = DateFormatter()
    
    var pos_transaction_header = PosTransactionHeader()
    var pos_customer = PosCustomers()
    //var pos_transaction_detail = [PosTransactionDetail]()
    //var pos_transaction_payment = PosTransactionPayment()
    
    
    
    var protocolDelegate : ModalQuickCashViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print
        let x_pos = (btnPrintReceipt.frame.size.width / 2) - 16
        
        let leftImageView1 = UIImageView()
        leftImageView1.image = UIImage(named: "print")
        leftImageView1.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnPrintReceipt.addSubview(leftImageView1)
        
        //no
        let leftImageView2 = UIImageView()
        leftImageView2.image = UIImage(named: "no")
        leftImageView2.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnNoReceipt.addSubview(leftImageView2)
        
        
        //cancel
        
        let leftImageView3 = UIImageView()
        leftImageView3.image = UIImage(named: "cancel")
        leftImageView3.frame = CGRect(x: x_pos, y: 5, width: 32, height: 32)
        
        btnCancel.addSubview(leftImageView3)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func btnPrintReceipt(_ sender: Any) {
        
        
        let response:Dictionary<String, Any> = self.PayQuickCash(with_receipt: true)
        
        let alert = UIAlertController(title: "Ticket", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            //self.dismiss(animated: false, completion: nil)
            if(response["success"]as? Bool)!{
                self.protocolDelegate?.didOpenModalQuickCash(viewController: self, success: true)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnNoReceipt(_ sender: Any) {
        
        
        
        let response:Dictionary<String, Any> = self.PayQuickCash(with_receipt: false)
        
        let alert = UIAlertController(title: "Ticket", message: response["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            //self.dismiss(animated: false, completion: nil)
            if(response["success"]as? Bool)!{
                //self.protocolDelegate?.didOpenTicket(viewController: self, success: true)
                
                self.protocolDelegate?.didOpenModalQuickCash(viewController: self, success: true)
                
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func PayQuickCash(with_receipt:Bool) -> Dictionary<String, Any> {
        
        var response:Dictionary<String, Any> = ["success":false,"message":"Error while saving payment"]
        
        let db_path = MyDefaults.getString("db_path")
        //let db_select = Database()
        
        format.dateFormat = "yyy-MM-dd HH:mm:ss"
        
        let new_date = format.string(from: date as Date)
        
        var cmd = ""
        var success:Bool = false
        
        let id = self.pos_transaction_header.id
        let line_numer = 1
        let payment_type_id = 1
        let card_numer = ""
        let reference_number = ""
        let amount = self.pos_transaction_header.total_net
        let tendered = self.pos_transaction_header.total_net
        let change = 0
        var ids = ""
        
        if let queue = FMDatabaseQueue(path: db_path) {
            queue.inTransaction() {
                db, rollback in
                
                
//                for detail in self.pos_transaction_detail {
//                    ids += detail.specialist_id + ","ll
//                }
//                
//                if(ids != ""){
//                    ids.remove(at: ids.index(before: ids.endIndex))
//                }
                
                    cmd = ""
                
                    //let rs:FMResultSet? = db?.executeQuery(cmd, withArgumentsIn: nil)
                
                    cmd = "update pos_transaction_header set status = 'P' where id = \(id)"
                
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                
                    cmd = "insert into pos_transaction_payment (id, line_number, payment_type_id, card_number, reference_number, amount, tendered, change) values ("
                    
                    cmd = cmd + "'\(id)',"
                    cmd = cmd + "'\(line_numer)',"
                    cmd = cmd + "'\(payment_type_id)',"
                    cmd = cmd + "'\(card_numer)',"
                    cmd = cmd + "'\(reference_number)',"
                    cmd = cmd + "'\(amount)',"
                    cmd = cmd + "'\(tendered)',"
                    cmd = cmd + "'\(change)'"
                    
                    cmd = cmd + ")"
                    
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                    
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
//                    success = (db?.executeUpdate("update specialist_on_queue set status = 'A', transaction_id = -1 where specialist_id = \(self.pos_transaction_header.specialist_id)", withArgumentsIn:nil))!
//                    
//                    if !success {
//                        rollback?.initialize(to: true)
//                        return
//                    }
                
                
                // get the trans specialist with service item only
                
                    cmd = "select *,count(main_category_id) as total_count, sum(item_net) as total_net from pos_transaction_detail where main_category_id = 1 and id = \(id) group by specialist_id"
                
                    var rs = FMResultSet()
                
                    rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                    while (rs.next()) {
                        //let main_category_id_count = rs?.string(forColumn: "main_category_id_count") ?? ""
                       // let main_category_id = rs.string(forColumn: "main_category_id") ?? ""
                        let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                        
                        //if(main_category_id == "1"){
                            ids += specialist_id + ","
                        //}
                        
                    }
                
                    if(ids != ""){
                        ids.remove(at: ids.index(before: ids.endIndex))
                    }
                // end
                
                // update the order number without the trans specialist
                
                cmd = "select * from specialist_on_queue where specialist_id not in (\(ids)) order by order_number asc"

                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                var i = 1
                
                while (rs.next()) {
                    let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                    
                    
                    success = (db?.executeUpdate("update specialist_on_queue set order_number = \(i) where specialist_id = \(specialist_id)", withArgumentsIn:nil))!
                    
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    i = i + 1
                }
                // end
                
                // update the order number of the trans specialist
//                cmd = "select * from specialist_on_que where specialist_id in (\(ids))"
//                
//                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
//                
//                while (rs.next()) {
//                    let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
//                    
//                    
//                    success = (db?.executeUpdate("update specialist_on_queue set order_number = \(i), status = 'A', transaction_id = -1 where specialist_id = \(specialist_id)", withArgumentsIn:nil))!
//                    
//                    if !success {
//                        rollback?.initialize(to: true)
//                        return
//                    }
//                    
//                    i = i + 1
//                }
                
                
                
                cmd = "select *,count(main_category_id) as total_count, sum(item_net) as total_net from pos_transaction_detail where main_category_id = 1 and id = \(id) group by specialist_id"
                
                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                while (rs.next()) {

                    let specialist_id = rs.string(forColumn: "specialist_id") ?? ""
                    let total_count = rs.string(forColumn: "total_count") ?? ""
                    let total_net = rs.string(forColumn: "total_net") ?? ""

                    success = (db?.executeUpdate("update specialist_on_queue set order_number = \(i), status = 'A', transaction_id = -1, total_count = total_count + \(total_count), total_amount = total_amount + \(total_net) where specialist_id = \(specialist_id)", withArgumentsIn:nil))!
                    
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    
                }
                //
                
//                first_visit_date
//                last_visit_date
//                visit_count
//                total_spent
                
                cmd = "select * from pos_customers where id = \(self.pos_customer.id)"
                rs = (db?.executeQuery(cmd, withArgumentsIn: nil))!
                
                while (rs.next()) {
                    
                    let first_visit_date = rs.string(forColumn: "first_visit_date") ?? ""
//                    let last_visit_date = rs.string(forColumn: "last_visit_date") ?? ""
//                    let visit_count = rs.string(forColumn: "visit_count") ?? "0"
//                    let total_spent = rs.string(forColumn: "total_spent") ?? "0"
                    
                    cmd = "update pos_customers set "
                    cmd = cmd + "last_visit_date = '\(new_date)',"
                    cmd = cmd + "total_spent = total_spent + \(self.pos_transaction_header.total_net),"
                    
                    if(first_visit_date == ""){
                        cmd = cmd + "first_visit_date = '\(new_date)',"
                    }
                    
                    cmd = cmd + "visit_count = visit_count + 1 "
                    
                    cmd = cmd + "where id = \(self.pos_customer.id) "
                    
                    success = (db?.executeUpdate(cmd, withArgumentsIn:nil))!
                    
                    if !success {
                        rollback?.initialize(to: true)
                        response["message"] = db?.lastErrorMessage()
                        return
                    }
                    
                    
                }
                
                return response = ["success":true,"message":"Ticket successfully paid."]
                

                
                
                
                
                } // end queue.inTransaction()
            } // end que if let queue

            return response
    } //end quick cash function
        
        
        
        

} // end class
